/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2016
*
*    FILE
*      $Workfile: SMS_LIB.cpp$
*      $Revision: 1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the SMS_LIB.c file
*
******************************************************************************/

/*============================================================================*
Private Defines
*============================================================================*/
#include "SMS_LIB.h"

/*============================================================================*
Public Defines
*============================================================================*/

/*============================================================================*
 Public Functions
*============================================================================*/


/*-----------------------------------------------------------------------------*
*  NAME
*      set_SMSC
*
*  DESCRIPTION
*      Checks for the the value of the variable, for the Mobile Station
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool SMS_LIB::set_SMSC(String phonenum)
{

 
  if (phonenum.compareTo(DEFAULT_SMSC) == 0) /*DEFAULT_SMSC equals SMSC_NUMBER*/
  {
    SMSC_NUMBER.concat("5555");
    return true;
  }
  
   else
     
      if (phonenum.length() <= MAX_PHONE_NUM_SIZE.length()) 
          {
            SMSC_NUMBER.concat(phonenum);
            return true;
          }
      if (phonenum.length() > MAX_PHONE_NUM_SIZE.length()) 
          {
           return false;
          }

}
/*-----------------------------------------------------------------------------*
*  NAME
*      set_Reciever
*
*  DESCRIPTION
*      Checks for the the value of the Reciver
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool SMS_LIB::set_Reciever(String phonenum)
{
  if (phonenum.compareTo(DEFAULT_RECEIVER) == 0) 
  {
    RECEIVER_NUMBER.concat("1234567");
    return true;
  }
  
   else
     
      if (phonenum.length() <= MAX_PHONE_NUM_SIZE.length()) 
          {
            RECEIVER_NUMBER.concat(phonenum);
            return true;
          }
      if (phonenum.length() > MAX_PHONE_NUM_SIZE.length()) 
          {
           return false;
          }
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      set_MSG
*
*  DESCRIPTION
*      Checks for the the value of the MSG
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool SMS_LIB::set_Reciever(String MSG)
{
  if (MSG.compareTo(DEFAULT_MSG) == 0) 
  {
    SMS_MSG.concat("Hello World!");
    return true;
  }
  
   else
     
      if (MSG.length() <= 159) 
          {
            SMS_MSG.concat(MSG);
            return true;
          }
      if (MSG.length() > 159) 
          {
           return false;
          }
   
}

/*-----------------------------------------------------------------------------*
*  NAME
*      input_attempts_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool SMS_LIB::input_attempts_number(int attemptsnum)
{
  
  ATTEMPTS = attemptsnum;

  if (ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
  {
    ATTEMPTS = DEFAULT_ATTEMPTS;
    return true;
  }
  if (ATTEMPTS > DEFAULT_ATTEMPTS)
  {
    return false;
  }
  else

    return true;
    
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_setup_pct
*
*  DESCRIPTION
*      Calulates the % of setupts
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float SMS_LIB::calc_setup_pct(float ttlsetups, float ttlattemps)
{
 
  if (ttlsetups == 0)
  {
    return 0;
  }
  else
    return ((ttlsetups / ttlattemps) * 100);

}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_connect_pct
*
*  DESCRIPTION
*      Calulates the % of connects
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float SMS_LIB::calc_connect_pct(float ttlconnect, float ttlattemps)
{
  if (ttlconnect == 0)
  {
    return 0;
  }
  else
    return ((ttlconnect / ttlattemps) * 100);
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_drop_pct
*
*  DESCRIPTION
*      Calulates the % of drops
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float SMS_LIB::calc_drop_pct(float ttldrop, float ttlattemps)
{
  if (ttldrop == 0)
  {
    return 0;
  }
  else
    return ((ttldrop / ttlattemps) * 100);
   
}

/*-----------------------------------------------------------------------------*
*  NAME
*      epoch_to_date_time
*
*  DESCRIPTION
*      Innput the number of seconds since the last Iridium epoch date. Sets the 
*      the grloval variables with year, month, day, hour, minute, seconds.
*  RETURNS
*      
*----------------------------------------------------------------------------*/
void SMS_LIB::epoch_to_date_time(uint32_t epoch)
{
 
    elapsed_second_s = epoch%60;    epoch /= 60;

    elapsed_minute_s = epoch%60;     epoch /= 60;

    elapsed_hour_s   = epoch%24;     epoch /= 24;

    float years = epoch/(365*4+1)*4;
    epoch %= 365*4+1;
   
   
    unsigned int year;
    
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    unsigned int month;
    
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }
   
    elapsed_year_s  = years+year;
    elapsed_month_s = month;
    elapsed_day_s   = (epoch-days[year][month]-1); //original code had no +1 
}
/*-----------------------------------------------------------------------------*
*  NAME
*      get_date_time
*
*  DESCRIPTION
*      Innput the countdown from the gloabal variables and output the current
*      date.
*  RETURNS
*      
*----------------------------------------------------------------------------*/
void SMS_LIB::get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss)
{
   /*Function Variables Declaration*/ 
uint8_t total_s = 0,
        total_m = 0, additional_m = 0,
        total_h = 0, additional_h = 0,
        total_d = 0, additional_d = 0,
        total_mo = 0, additional_mo = 0,
        additional_y = 0;
uint16_t total_y = 0;        
  
/*CALCULATE SECONDS*/
    total_s = 55 + sss;
   //Checks if the addition >= 60s
   //If so a minute is carried and added.
  if(total_s > 59)
    {
     additional_m = 1;
     s = total_s - 60;
    }
    else 
    {
     additional_m = 0;
     s = total_s;
    }
 /*END CALCULATE SECONDS*/
 /*CALCULATE MINUTES*/  
   //This condition will never be meet because minute_s is always <= 36 m
  // Just in case the 23 is ever increased this condition will be meet.
    if(additional_m == 1)
    {
        total_m = mmm + 23 + 1;
       
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          m = total_m;
          additional_h = 0;
        }
             
    }
    if(additional_m == 0)
    {

      total_m = mmm + 23;
      
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          additional_h = 0;
          m = total_m;
        }
    }    
 /*END CALCULATE MINUTES*/
 /*CALCULATE HOURS*/
    if(additional_h == 1)
    {
    
      total_h = hhh + 14 + 1;
      
      if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 1; // an day is added
        }
        else
        {
          h = total_h;
          additional_d = 0;
        }
             
    }
    if(additional_h == 0)
    {
      total_h = hhh + 14;
        if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 0; // an hour is added

        }
        else
        {
          additional_d = 0;
          h = total_h;
        }
    } 
    /*END CALCULATE HOURS*/
    /*CALCULATE DAY & MONTH*/
    if(additional_d == 1)
    {
      total_d = ddd + 11 + 1;

      if(total_d > 31)
      {
        d = total_d - 31;
        additional_mo = 1;
        
       }
          else
            {
              d = total_d;
              additional_mo = 0;
            
            }
    }
    if(additional_d == 0)
    {
      total_d = ddd + 11;
   
      if(total_d > 31)
      {
             
        d = total_d - 31;
        additional_mo = 1;
      
       }
          else
            {
              d = total_d;
              additional_mo = 0;
              
            }

    }    




    

    if(additional_mo == 1)
    {
      total_mo = mmm_o + 5 +1;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }                
       }
            else
            {
               mo = total_mo;
               additional_y = 0;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
            }

      
    }
    
    if(additional_mo == 0)
    {
      
    total_mo = mmm_o + 5;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }        

      }
            else
            {
               mo = total_mo;
               additional_y = 0;


             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
  
            }
      
    }

    /*END CALCULATE DAY & MONTH/
    /*CALCULATE YEAR*/
     if(additional_y == 1)
     {
      
      y = 14 + 1 + yyy;
      total_y = 2000 + y;
 
        //Check for leap year and change date only for Feb

             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }


        


        
     }
     if(additional_y == 0)
     {
        
        y = 14 + yyy;
        total_y = 2000 + y; 

        //Check for leap year and change date only for Feb
             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }

       
     }

    
    /*END CALCULATE YEAR*/


   

    
    /*END CALCULATE DATE&TIME*/
  UTC_TIME.concat("20");
  UTC_TIME.concat(y);
  UTC_TIME.concat("/");
  UTC_TIME.concat(mo);
  UTC_TIME.concat("/");
  UTC_TIME.concat(d);
  UTC_TIME.concat(" ");
  UTC_TIME.concat(h);
  UTC_TIME.concat(":");
  UTC_TIME.concat(m);
  UTC_TIME.concat(":");
  UTC_TIME.concat(s);
    
    
}
/*-----------------------------------------------------------------------------*
*  NAME
*      set_UTC
*
*  DESCRIPTION
*       Input the returend value from the -MSSTM command and the final output will 
*       be the UTC_TIME string equal to the current time.
*  RETURNS
*
*----------------------------------------------------------------------------*/  
  void SMS_LIB::set_UTC(double epoch_val)
  {
     uint32_t epoch = epoch_val * 0.09;
     
     LFRAME.remove(0);
     LFRAME.concat(String(epoch_val,0));
     
     epoch_to_date_time(epoch);
     
     get_date_time(elapsed_year_s,elapsed_month_s,elapsed_day_s,elapsed_hour_s,elapsed_minute_s,elapsed_second_s);
    
     
  }
/*-----------------------------------------------------------------------------*
*  NAME
*      SMS_command_setup
*
*  DESCRIPTION
*       Sets the global variables for set up to their corresponding values 
*   
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::SMS_commands_setup(void)
{
  
  ADD_TYPE_AT.remove(0);
  MSG_FORMAT_AT.remove(0);
  SMSC_ADD_AT.remove(0);

  ADD_TYPE_AT.concat("AT+CSTA=129\r");
  MSG_FORMAT_AT.concat("AT+CMGF=0\r");
  SMSC_ADD_AT.concat("AT+CSCA=\"");
  SMSC_ADD_AT.concat(SMSC_NUMBER);
  SMSC_ADD_AT.concat("\"\r");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      SMS_command_setup
*
*  DESCRIPTION
*       Sets the global variables for set up to their corresponding values 
*   
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::SMS_commands_setup(void)
{
  
  ADD_TYPE_AT.remove(0);
  MSG_FORMAT_AT.remove(0);
  SMSC_ADD_AT.remove(0);

  ADD_TYPE_AT.concat("AT+CSTA=129\r");
  MSG_FORMAT_AT.concat("AT+CMGF=0\r");
  SMSC_ADD_AT.concat("AT+CSCA=\"");
  SMSC_ADD_AT.concat(SMSC_NUMBER);
  SMSC_ADD_AT.concat("\"\r");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      PDU_MSG_generate
*
*  DESCRIPTION
*        Converts ADD_TYPE_AT, MSG_FORMAT_AT and SMSC_ADD_AT into pdu format msg.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::PDU_MSG_generate(void)
{
    if (SMS_MSG.compareTo(DEFAULT_MSG) == 0) 
      {
           if (RECEIVER_NUMBER.compareTo(DEFAULT_RECEIVER) == 0) 
              {
                 if (SMSC_NUMBER.compareTo(DEFAULT_SMSC) == 0) 
                   {
                     PDU_MSG.concat("0381555511000781214365F70000AA0CC8329BFD065DDF72363904");
                     //after this is sent send DF_Port.write(0x1A); //0x1A = 26 = ctrl+z
                    }
              }
      }
      else
      {
        
        
        
        
        
        
        
      }
   }
/*-----------------------------------------------------------------------------*
*  NAME
*      PDU_MSG_send
*
*  DESCRIPTION
*        AT command with the size of the PDU message concatinated 
*        to the MSG_SEND_AT string.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::PDU_MSG_send(void)
{
   MSG_SEND_AT.remove(0);
   
   MSG_SEND_AT.concat("AT+CMGS=");
   MSG_SEND_AT.concat(PDU_MSG_SIZE);
   MSG_SEND_AT.concat("\r");
   
      
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      clear_data_array
*
*  DESCRIPTION
*        Sets all the values in DATA_ARRAY[] to NULL.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::clear_data_array(void)
{
  DATA_ARRAY.remove(0);
  DATA_ARRAY_TWO.remove(0);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      init_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the header content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::init_logfile_string(void)
{
   clear_data_array();

   DATA_ARRAY.concat("Log File\nTest Start Time  (UTC):  ");
   DATA_ARRAY.concat(UTC_TIME);
   DATA_ARRAY.concat("\n\n\nUTC Time              ");
   DATA_ARRAY.concat("LFrame      Phone Number      Att#  SVID  BeamID  ");
   DATA_ARRAY_TWO.concat("Access  Access     Setup  Setup      Comp  Duration  LGCX   LGCY   LGCZ\n");
   DATA_ARRAY_TWO.concat("                                                          Init  Init    ");
   DATA_ARRAY_TWO.concat("        Time(sec)         Time(sec)        (sec)\n");
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      logfile_upload_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the log content of the log
*    file. The input att represents the current att number. 
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::logfile_upload_string(int att)
{
  

  clear_data_array();
  
  
  DATA_ARRAY.concat(UTC_TIME);
  DATA_ARRAY.concat("    ");
  
  DATA_ARRAY.concat(LFRAME);
  for(int i=0;i<(12-(LFRAME.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(SMSC_NUMBER);
  for(int i=0;i<(18-(SMSC_NUMBER.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(String(att));
  for(int i=0;i<(6-((String(att)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(SVID);
  for(int i=0;i<(6-(SVID.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(BeamID);
  for(int i=0;i<(8-(BeamID.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(ACC);
  DATA_ARRAY.concat("       ");
  
  DATA_ARRAY.concat(String(ACCESS_TIME,2));
  for(int i=0;i<(11-((String(ACCESS_TIME,2)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(SET);
  DATA_ARRAY.concat("      ");
  
  DATA_ARRAY.concat(String(SETUP_TIME,2));
  for(int i=0;i<(11-((String(SETUP_TIME,2)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY_TWO.concat(COMP);
  DATA_ARRAY_TWO.concat("     ");
  DATA_ARRAY_TWO.concat(String(DURATION_TIME,2));
  for(int i=0;i<(9-((String(DURATION_TIME,2)).length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }


  DATA_ARRAY_TWO.concat(LGCX);
  for(int i=0;i<(7-(LGCX.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
  DATA_ARRAY_TWO.concat(LGCY);
  for(int i=0;i<(7-(LGCY.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
  DATA_ARRAY_TWO.concat(LGCZ);
  for(int i=0;i<(7-(LGCZ.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
 DATA_ARRAY_TWO.concat("\n");
}
/*-----------------------------------------------------------------------------*
*  NAME
*      end_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the summary content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void SMS_LIB::end_logfile_string(void)
{
  
  

  clear_data_array();
  
  DATA_ARRAY.concat("\n\n\n\nTest Stop:    ");
  DATA_ARRAY.concat(UTC_TIME);
  DATA_ARRAY.concat("\n-----------------------\n");

  DATA_ARRAY.concat("Attempts      =    ");
  DATA_ARRAY.concat(String(ATTEMPTS));
  DATA_ARRAY.concat("\n");
  
  DATA_ARRAY.concat("Setups        =    ");
  DATA_ARRAY.concat(String(SETUPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Setup Pct     =  ");
  DATA_ARRAY.concat(String(SETUPS_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Connects      =    ");
  DATA_ARRAY.concat(String(CONNECTS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Connect Pct   =  ");
  DATA_ARRAY.concat(String(CONNECT_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Drops         =    ");
  DATA_ARRAY.concat(String(DROPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Drop Pct      =  ");
  DATA_ARRAY.concat(String(DROPS_PCT,2));
  DATA_ARRAY.concat("%\n");
  DATA_ARRAY.concat("\n");
  
}
