
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H

#include "windows.h"
#include "winbase.h"
#include <string>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <cstring>

/*============================================================================*
Private Defines
*============================================================================*/
#define TPI_PARAM_IDX (TPI_OPCODE_IDX+TPI_OPCODE_SIZE)
#define TPI_OPCODE_IDX 0
#define TPI_OPCODE_SIZE 1

/*============================================================================*
Public Defines
*============================================================================*/
#define DEFAULT_CALL_TIME	50
#define DEFAULT_ATTEMPTS   1000
#define MAX_CALL_TIME_SIZE 32767
#define TPI_RRCS_SV_ID_BYTE (TPI_PARAM_IDX + 8)
#define TPI_RRCS_BEAM_ID_BYTE (TPI_PARAM_IDX + 9)
#define TPI_RRCS_X_COORD_WORD (TPI_PARAM_IDX + 10)
#define TPI_RRCS_Y_COORD_WORD (TPI_PARAM_IDX + 12)
#define TPI_RRCS_Z_COORD_WORD (TPI_PARAM_IDX + 14) 

#ifdef __cplusplus 
extern "C" {
#endif 

	float 
		SETUPS = 0,
		CONNECTS = 0,
		CONNECT_PCT = 0,
		DROPS = 0,
		DROPS_PCT = 0,
		ACCESS_TIME = 0,
		DURATION_TIME = 0,
		SETUP_TIME = 0,
		CALL_TIME = 0, 
		ATTEMPTS = 0,
		SETUPS_PCT = 0;

	bool COMPLETE = false, SETUP = false;

	char UTC_TIME_DEFAULT[] = "2001/01/01 01:01:01",
		UTC_TIME[] = "                   ",
		PHONE_NUMBER[] = "             ",
	    MAX_PHONE_NUM_SIZE[] = "9999999999";

	const char DEFAULT_PHONE_NUMBER[] = "5555";

	char DATA_ARRAY[255];

	time_t start_t_access, end_t_access,
		start_t_setup, end_t_setup,
		start_t_duration, end_t_duration;

	short diff_t_access,
		diff_t_setup,
		diff_t_duration;

	/*============================================================================*
	Public Functions
	*============================================================================*/
	bool input_phone_number(char phonenum[]);
	bool input_call_time(float calltime);
	bool input_utc_time(char utctime[]);
	bool input_attempts_number(float attemptsnum);
	float calc_setup_pct(float ttlsetups, float ttlattemps);
	float calc_connect_pct(float ttlconnect, float ttlattemps);
	float calc_drop_pct(float ttldrop, float ttlconnect);
	void access_watch_start(void);
	void setup_watch_start(void);
	void duration_watch_start(void);
	int access_watch_stop(void);
	int setup_watch_stop(void);
	int duration_watch_stop(void);



#ifdef __cplusplus 
}
#endif 

#endif /* _VOICE_TEST_H*/ 