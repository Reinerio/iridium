#include <voice_test.h>
#include<CountUpDownTimer.h>
#include <SoftwareSerial.h>

//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
SoftwareSerial mySerial(7,6); //SoftwareSerial mySerial(RX, TX);
CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 
voicetest vt;

int tries = 0;
String RX_buffer = "";
  
void setup() 
{
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
    // set the data rate for the SoftwareSerial port
  mySerial.begin(115200);


  t_a.StartTimer();
  t_s.StartTimer();
  t_d.StartTimer(); 

  do
  {
    /*Send AT to recive OK responce*/
    mySerial.listen(); 
    mySerial.write("AT\r"); 
    Serial.println("AT send");
    
    if (mySerial.available()> 0)
      {
          Serial.print("RX buffer: ");
          RX_buffer = mySerial.readStringUntil('\n');
          Serial.println(RX_buffer);

              if(RX_buffer.substring(0,2) == "OK")
              {
                   RX_buffer.remove(0);
                   RX_buffer.concat("OK");
              }
              else
              {
                   RX_buffer.remove(0);
              }
      }
  }while(RX_buffer != "OK");

  RX_buffer.remove(0);

  vt.input_phone_number("12345678");
  vt.input_call_time(5);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(5);
  vt.BeamID = "12";
  vt.SVID = "09";
  vt.ACC = "Y";
  vt.SET = "Y";
  vt.COMP = "Y";
  vt.LGCX = "107.5";
  vt.LGCY = "98.3";
  vt.LGCZ = "104.3";

  vt.ISU_RTS_SET();

  do
  {
    /*Set Time*/
    mySerial.listen(); 
    mySerial.write(vt.RTC); 
    Serial.println("+CCLK command send");
    
    if (mySerial.available()> 0)
      {
          Serial.print("RX buffer: ");
          RX_buffer = mySerial.readStringUntil('\n');
          Serial.println(RX_buffer);

              if(RX_buffer.substring(0,5) == "ERROR")
              {
                   RX_buffer.remove(0);
                   Serial.println("Error detected please check UTC time syntax \"yy/mm/dd,hh:mm:ss\"");
              }
              if(RX_buffer.substring(0,2) == "OK")
              {
                   RX_buffer.remove(0);
                   RX_buffer.concat("OK");                
              }
              else
              {
                   RX_buffer.remove(0);
              }
      }
  }while(RX_buffer != "OK");

}

void loop()
{
  int attempt_number = 0;
  /*Read RTC from ISU and set the RTC time variable to it*/
  do
  {
    /*Set Time*/
    vt.UTC_TIME.remove(0);
    mySerial.listen(); 
    mySerial.write("AT+CCLK\r"); 
    Serial.println("+CCLK command send");
    
    if (mySerial.available()> 0)
      {
          Serial.print("RX buffer: ");
          RX_buffer = mySerial.readStringUntil('+');
          Serial.println(RX_buffer);

//Make sure that the correct data is outputed in this format CCLK:07/03/08,03:50:21
//This is to make sure no noice is read
    if(RX_buffer.substring(0,5) == "CCLK:")
    {
            
           if(RX_buffer.substring(7,8) == "/")
            {
           if(RX_buffer.substring(10,11) == "/")
            {
           if(RX_buffer.substring(13,14) == ",")
            {
           if(RX_buffer.substring(16,17) == ":")
            {   
           if(RX_buffer.substring(19,20) == ":")
            { 
                  for(int i = 0;i<17;i++)
                  {
                    char mostSignificantDigit = RX_buffer.charAt(i+5);
                    vt.UTC_TIME.concat(mostSignificantDigit); 
                    Serial.println(mostSignificantDigit);
                  }                
            }
            }
            } 
            } 
            }

         Serial.println(vt.UTC_TIME);
       }
     }
  }while(RX_buffer != "OK");

   if(RX_buffer == "OK")
     {
       vt.SET = "Y";
       vt.SETUPS++;
     }
      else
      {
        vt.SET = "N";
      } 
      RX_buffer.remove(0);
  
  /*Get UTC updated every time you log into file*/
 

  vt.init_logfile_string();
  Serial.print(vt.DATA_ARRAY);
  t_s.StopTimer(); 
  
  for(int i=0;i<vt.ATTEMPTS;i++)
  { 
    
  t_s.Timer();
  t_a.Timer();
  
  
  vt.SETUP_TIME = t_s.ShowSeconds(); 
  t_s.StopTimer(); 

  t_a.Timer();
  
  do
  {
    
  if (Serial.available() > 0)
  {
    Serial.write(vt.DIAL);
    RX_buffer = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(RX_buffer);
  }
      if(RX_buffer == "OK")
      {
        vt.ACC = "Y";
        vt.CONNECTS++;
      }
      if(RX_buffer == "NO CARRIER")
      {
        vt.ACC = "N";
        vt.DROPS++;
      }

      
  } while(RX_buffer != "OK");

  RX_buffer.remove(0);
  
  vt.ACCESS_TIME = t_a.ShowSeconds(); 
  t_a.StopTimer(); 
   
  
  t_d.Timer();
  if(vt.ACC == "Y")
  {
    
   while(vt.DURATION_TIME != vt.CALL_TIME)
    {
      if (t_d.TimeHasChanged())
      {
         vt.DURATION_TIME = t_d.ShowSeconds(); 
      }
    }

  if (Serial.available() > 0)
  {
    Serial.write("ATH");
    RX_buffer = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(RX_buffer);
  }
  
  } while(RX_buffer != "OK");
  RX_buffer.remove(0);
  
  vt.DURATION_TIME = t_d.ShowSeconds(); 
  t_d.StopTimer();
  
    vt.COMP = "Y";
  
  if(vt.ACC == "N")
    {
       vt.COMP = "N";      
    }
  
  /*
  * In this section of the code we will get the BEAMID SVID, X, Y, Z and frame 
  * values from the DPL PORT. For testing we will use the pre-defined values
  * set on the setup() function.
   */
   
  /*Get UTC updated every time you log into file*/
  do
  {
  if (Serial.available() > 0)
  {
    Serial.write("AT+CCLK?");
    RX_buffer = Serial.readString();
    Serial.print("UPDATE UTC time sent, received: ");
    Serial.println(RX_buffer);
  }
  } while(RX_buffer != "OK"); 
  
  vt.RTC.concat(RX_buffer);
  RX_buffer.remove(0);

  
   vt.logfile_upload_string(attempt_number);
   Serial.println(vt.DATA_ARRAY);
 //  T.ResetTimer(); 
   vt.ACCESS_TIME = 0;
   vt.DURATION_TIME = 0;
   vt.SETUP_TIME = 0;

    attempt_number++;
  }
  
  /*Get UTC updated every time you log into file*/
  do
  {
  if (Serial.available() > 0)
  {
    Serial.write("AT+CCLK?");
    RX_buffer = Serial.readString();
    Serial.print("UPDATE UTC time sent, received: ");
    Serial.println(RX_buffer);
  }
  }while(RX_buffer != "OK"); 
  
  vt.RTC.concat(RX_buffer);
  RX_buffer.remove(0);


  vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
  vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
  vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.CONNECTS);
  
  vt.end_logfile_string();
  Serial.print(vt.DATA_ARRAY);  













}




