#include <CountUpDownTimer.h>

#include <SoftwareSerial.h>

String DF_RX_buffer = "";
String dumDF_RX_buffer = "Hello World";
int set_up = 0, access = 0, duration = 0;
//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX

SoftwareSerial DF_Port(10,6); //SoftwareSerial mySerial(RX, TX);
void get_AT_Commands(void);
CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 

void setup() {
  
  // initialize serial:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DF_Port.begin(115200);

}

void loop() {

    DF_Port.listen(); //Enables the port to listen 
    //Send AT command
   
       t_a.StartTimer();
       t_s.StartTimer();
       t_d.StartTimer(); 
       
      
       Serial.println("Sart Timer"); 
       
       get_AT_Commands('S');
       set_up = (t_s.ShowMilliSeconds()/1000) % 3600; 
       Serial.print("Set up Time: ");
       Serial.println(set_up);
       set_up = 0;
       t_s.StopTimer(); 

       
       get_AT_Commands('A');
       access = (t_a.ShowMilliSeconds()/1000) % 3600;  
       Serial.print("Access Time: ");
       Serial.println(access); 
       access = 0;
       t_a.StopTimer(); 
       
       
          
       get_AT_Commands('D');
       duration = (t_d.ShowMilliSeconds()/1000) % 3600;  
       Serial.print("Duration Time: ");
       Serial.println(duration);     
       duration = 0;     
       t_d.StopTimer();


          
}

void get_AT_Commands(char timer)
{
   
   do
   {
   DF_Port.write("AT+CCLK?\r"); 

     if(timer == 'S')
     {
      t_s.Timer();
     }
     if(timer == 'A')
     {
      t_a.Timer();
     }
     if(timer == 'D')
     {
      t_d.Timer();
     }


    //If > 0 means that there's data on the buffer
    while (DF_Port.available()>0)
    {
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
    // Serial.print("Rx recived Data: ");  
     DF_RX_buffer = DF_Port.readStringUntil('+');
    }
    //Serial.println(DF_RX_buffer);
//       set_up = (t_s.ShowMilliSeconds()/1000) % 3600; 
//       Serial.print("Set up Time: ");
//       Serial.println(set_up);
//       access = (t_a.ShowMilliSeconds()/1000) % 3600;  
//       Serial.print("Access Time: ");
//       Serial.println(access); 
//       duration = (t_d.ShowMilliSeconds()/1000) % 3600;  
//       Serial.print("Duration Time: ");
//       Serial.println(duration); 
    
    if(DF_RX_buffer.indexOf("CCLK") >= 0)
    {
        int index = DF_RX_buffer.indexOf("/"), //finds the first '/'
            index_2 = DF_RX_buffer.indexOf(","), //finds the first ','
            index_2_1 = DF_RX_buffer.indexOf(":"), //finds the first ':'
            index_3 = DF_RX_buffer.indexOf(":",index_2_1+1); //finds the second ':'
            
          if(DF_RX_buffer.indexOf("/",(index+1)) == (index+3))
            {// Serial.println("found th second '/'");
           if(index_2 == 13)
            { //Serial.println("found the ','");
           if(DF_RX_buffer.indexOf(":",(index_3+1)) == (index_3+3))
            { 

                  for(int i = 0;i<17;i++)
                  {
                    char mostSignificantDigit = DF_RX_buffer.charAt(i+5);
                    dumDF_RX_buffer.concat(mostSignificantDigit); 
                    //Serial.println(mostSignificantDigit);
                  }     
                  Serial.println("Time:  ");
                  Serial.println(dumDF_RX_buffer); 
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");              
            
            }
            } 
            }
    
       }


//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();

    //DF_RX_buffer.remove(0);
    dumDF_RX_buffer.remove(0);
   // Serial.print("Buffer Cleared"); 
    //Serial.println(DF_RX_buffer); 
    


//    if (DF_Port.available()<=0)
//    {
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();   
      // Serial.println("No DATA on RX buffer");
//    }

//      t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
    

    //wait 3 s and send data again 
    delay(20);
   }while(DF_RX_buffer != "OK");
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
   DF_RX_buffer.remove(0);
   
}

