
uint32_t epoch = 78790846;

float elapsed_second_s = 0, // 0-59
        elapsed_minute_s = 0, // 0-59
        elapsed_hour_s = 0,   // 0-23
        elapsed_day_s = 0,
        elapsed_month_s = 0,  // 1-12
        elapsed_year_s = 0;   // 00-99 (representing 2000-2099)

uint8_t y = 0,
        mo = 0,
        d = 0,
        h = 0,
        m = 0,
        s = 0;
        

static unsigned short days[4][12] =
{
    {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
    { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
    { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
    {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
};

String UTC_t = "";

void epoch_to_date_time(uint32_t epoch);
void get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss);


void setup() 
{


  Serial.begin(115200);

  
  
  Serial.print("epoch: ");
  Serial.println(epoch);
  
  epoch_to_date_time(epoch); 
   
  Serial.print("Countdown: ");
  Serial.print(elapsed_year_s);
  Serial.print("y ");
  Serial.print(elapsed_month_s);
  Serial.print("m ");
  Serial.print(elapsed_day_s);
  Serial.print("d ");
  Serial.print(elapsed_hour_s);
  Serial.print("h ");
  Serial.print(elapsed_minute_s);
  Serial.print("m ");
  Serial.print(elapsed_second_s);
  Serial.println("s. ");

  get_date_time(elapsed_year_s,elapsed_month_s,elapsed_day_s,elapsed_hour_s,elapsed_minute_s,elapsed_second_s);

  Serial.print("Current Date: ");
  Serial.print(y);
  Serial.print("/");
  Serial.print(mo);
  Serial.print("/");
  Serial.print(d);
  Serial.print("  ");
  Serial.print(h);
  Serial.print(":");
  Serial.print(m);
  Serial.print(":");
  Serial.print(s);
  Serial.println(" ");

  UTC_t.concat("20");
  UTC_t.concat(y);
  UTC_t.concat("/");
  UTC_t.concat(mo);
  UTC_t.concat("/");
  UTC_t.concat(d);
  UTC_t.concat(" ");
  UTC_t.concat(h);
  UTC_t.concat(":");
  UTC_t.concat(m);
  UTC_t.concat(":");
  UTC_t.concat(s);
  

  Serial.println(UTC_t);
 
  
  

   
}

void loop() 
{


     
  delay(2000);
}

void epoch_to_date_time(uint32_t epoch)
{
  

    elapsed_second_s = epoch%60;    epoch /= 60;

    elapsed_minute_s = epoch%60;     epoch /= 60;

    elapsed_hour_s   = epoch%24;     epoch /= 24;

    float years = epoch/(365*4+1)*4;
    epoch %= 365*4+1;
   
   
    unsigned int year;
    
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    unsigned int month;
    
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }
   
    elapsed_year_s  = years+year;
    elapsed_month_s = month;
    elapsed_day_s   = (epoch-days[year][month]-1); //original code had no +1
}


void get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss)
{
  /*Function Variables Declaration*/ 
uint8_t total_s = 0,
        total_m = 0, additional_m = 0,
        total_h = 0, additional_h = 0,
        total_d = 0, additional_d = 0,
        total_mo = 0, additional_mo = 0,
        additional_y = 0;
uint16_t total_y = 0;        
  
/*CALCULATE SECONDS*/
    total_s = 55 + sss;
   //Checks if the addition >= 60s
   //If so a minute is carried and added.
  if(total_s > 59)
    {
     additional_m = 1;
     s = total_s - 60;
    }
    else 
    {
     additional_m = 0;
     s = total_s;
    }
 /*END CALCULATE SECONDS*/
 /*CALCULATE MINUTES*/  
   //This condition will never be meet because minute_s is always <= 36 m
  // Just in case the 23 is ever increased this condition will be meet.
    if(additional_m == 1)
    {
        total_m = mmm + 23 + 1;
       
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          m = total_m;
          additional_h = 0;
        }
             
    }
    if(additional_m == 0)
    {

      total_m = mmm + 23;
      
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          additional_h = 0;
          m = total_m;
        }
    }    
 /*END CALCULATE MINUTES*/
 /*CALCULATE HOURS*/
    if(additional_h == 1)
    {
    
      total_h = hhh + 14 + 1;
      
      if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 1; // an day is added
        }
        else
        {
          h = total_h;
          additional_d = 0;
        }
             
    }
    if(additional_h == 0)
    {
      total_h = hhh + 14;
        if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 0; // an hour is added

        }
        else
        {
          additional_d = 0;
          h = total_h;
        }
    } 
    /*END CALCULATE HOURS*/
    /*CALCULATE DAY & MONTH*/
    if(additional_d == 1)
    {
      total_d = ddd + 11 + 1;

      if(total_d > 31)
      {
        d = total_d - 31;
        additional_mo = 1;
        
       }
          else
            {
              d = total_d;
              additional_mo = 0;
            
            }
    }
    if(additional_d == 0)
    {
      total_d = ddd + 11;
   
      if(total_d > 31)
      {
             
        d = total_d - 31;
        additional_mo = 1;
      
       }
          else
            {
              d = total_d;
              additional_mo = 0;
              
            }

    }    




    

    if(additional_mo == 1)
    {
      total_mo = mmm_o + 5 +1;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }                
       }
            else
            {
               mo = total_mo;
               additional_y = 0;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
            }

      
    }
    
    if(additional_mo == 0)
    {
      
    total_mo = mmm_o + 5;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }        

      }
            else
            {
               mo = total_mo;
               additional_y = 0;


             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
  
            }
      
    }

    /*END CALCULATE DAY & MONTH/
    /*CALCULATE YEAR*/
     if(additional_y == 1)
     {
      
      y = 14 + 1 + yyy;
      total_y = 2000 + y;
 
        //Check for leap year and change date only for Feb

             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }


        


        
     }
     if(additional_y == 0)
     {
        
        y = 14 + yyy;
        total_y = 2000 + y; 

        //Check for leap year and change date only for Feb
             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }

       
     }

    
    /*END CALCULATE YEAR*/



    
}



