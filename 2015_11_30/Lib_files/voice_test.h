/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.h$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H

#include <String.h>
#include <WString.h>
//#include <Chrono.h>
//#include "utility/CountUpDownTimer.h"
//#include "CountUpDownTimer.h"

/*============================================================================*
Private Defines
*============================================================================*/
#define TPI_PARAM_IDX (TPI_OPCODE_IDX+TPI_OPCODE_SIZE)
#define TPI_OPCODE_IDX 0
#define TPI_OPCODE_SIZE 1

/*============================================================================*
Public Defines
*============================================================================*/
#define DEFAULT_CALL_TIME  50
#define DEFAULT_ATTEMPTS   1000
#define MAX_CALL_TIME_SIZE 32767
#define TPI_RRCS_SV_ID_BYTE (TPI_PARAM_IDX + 8)
#define TPI_RRCS_BEAM_ID_BYTE (TPI_PARAM_IDX + 9)
#define TPI_RRCS_X_COORD_WORD (TPI_PARAM_IDX + 10)
#define TPI_RRCS_Y_COORD_WORD (TPI_PARAM_IDX + 12)
#define TPI_RRCS_Z_COORD_WORD (TPI_PARAM_IDX + 14) 

/*CountUpDownTimer  t_a(UP),
                  t_s(UP),
                  t_d(UP);
*/
class voicetest{

 private:

   String UTC_TIME_DEFAULT = "YY/MM/DD,HH:MM:SS",
          MAX_PHONE_NUM_SIZE = "9999999999",
          DEFAULT_PHONE_NUMBER = "5555";
  
  public:
  
  int ATTEMPTS = 0, SETUPS = 0, CONNECTS = 0, DROPS = 0;
 
  float
    CONNECT_PCT = 0,
    DROPS_PCT = 0,
    SETUPS_PCT = 0,
    ACCESS_TIME = 0,
    DURATION_TIME = 0,
    SETUP_TIME = 0,
    CALL_TIME = 0;
  
  bool COMPLETE = false, SETUP = false;
  
  String UTC_TIME = "",
    LFRAME = "",
    PHONE_NUMBER = "",
    attempp = "",
    BeamID = "",
    SVID = "",
    ACC = "",
    COMP = "",
    SET = "",
    ABTERM = "",
    LGCX = "",
    LGCY = "",
    LGCZ = "",
    DIAL = "",
    RTC = "",
    DATA_ARRAY = "";

  /*Chrono diff_t_access,
    diff_t_setup,
    diff_t_duration;
   */
  /*============================================================================*
  Public Functions
  *============================================================================*/
  bool input_phone_number(String phonenum);
  bool input_call_time(float calltime);
  bool input_utc_time(String utctime);
  bool input_attempts_number(int attemptsnum);
  float calc_setup_pct(float ttlsetups, float ttlattemps);
  float calc_connect_pct(float ttlconnect, float ttlattemps);
  float calc_drop_pct(float ttldrop, float ttlconnect);

  void start_timers(void);
  void access_watch_start(void);
  void setup_watch_start(void);
  void duration_watch_start(void);
  void access_watch_stop(void);
  void setup_watch_stop(void);
  void duration_watch_stop(void);

  void voice_call_dial(void);
  void ISU_RTC_SET(void);

  void clear_data_array(void);
  void init_logfile_string(void);
  void end_logfile_string(void);
  void logfile_upload_string(int att);


         
};

#endif /* _VOICE_TEST_H*/ 



