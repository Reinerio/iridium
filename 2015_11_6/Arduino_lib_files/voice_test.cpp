/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.cpp$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/

/*============================================================================*
Private Defines
*============================================================================*/
#include "voice_test.h"

/*============================================================================*
Public Defines
*============================================================================*/

/*============================================================================*
 Public Functions
*============================================================================*/


/*-----------------------------------------------------------------------------*
*  NAME
*      input_phone_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool voicetest::input_phone_number(String phonenum)
{

 
  if (phonenum.compareTo(DEFAULT_PHONE_NUMBER) == 0) /*DEFAULT_PHONE_NUMBER equals PHONE_NUMBER*/
  {
    PHONE_NUMBER.concat("5555");
    return true;
  }
  
   else
     
      if (PHONE_NUMBER.length() <= MAX_PHONE_NUM_SIZE.length()) /*PHONE_NUMBER is less than MAX_PHONE_NUM_SIZE*/
          {
            PHONE_NUMBER.concat(phonenum);
            return true;
          }
      if (PHONE_NUMBER.length() > MAX_PHONE_NUM_SIZE.length()) /*MAX_PHONE_NUM_SIZE is less than PHONE_NUMBER*/
          {
           return false;
          }

}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_call_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool voicetest::input_call_time(float calltime)
{
  CALL_TIME = calltime;

  if (CALL_TIME == 0 || CALL_TIME == DEFAULT_CALL_TIME)
  {
    CALL_TIME = DEFAULT_CALL_TIME;
    return true;
  }
  if (CALL_TIME > MAX_CALL_TIME_SIZE)
  {
    return false;
  }
  else
   
    return true;
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_utc_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool voicetest::input_utc_time(String utctime)
{
   UTC_TIME.concat(utctime);
 
  if (UTC_TIME.length() ==  UTC_TIME_DEFAULT.length())
  {
    return true;
  }
  else
    return false;


}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_attempts_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool voicetest::input_attempts_number(int attemptsnum)
{
  
  ATTEMPTS = attemptsnum;

  if (ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
  {
    ATTEMPTS = DEFAULT_ATTEMPTS;
    return true;
  }
  if (ATTEMPTS > DEFAULT_ATTEMPTS)
  {
    return false;
  }
  else

    return true;
    
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_setup_pct
*
*  DESCRIPTION
*      Calulates the % of setupts
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_setup_pct(float ttlsetups, float ttlattemps)
{
 
  if (ttlsetups == 0 || ttlattemps == 0)
  {
    return 0;
  }
  else
    return ((ttlsetups / ttlattemps) * 100);

}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_connect_pct
*
*  DESCRIPTION
*      Calulates the % of connects
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_connect_pct(float ttlconnect, float ttlattemps)
{
  if (ttlconnect == 0 || ttlattemps == 0)
  {
    return 0;
  }
  else
    return ((ttlconnect / ttlattemps) * 100);
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_drop_pct
*
*  DESCRIPTION
*      Calulates the % of drops
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_drop_pct(float ttldrop, float ttlconnect)
{
  if (ttldrop == 0 || ttlconnect == 0)
  {
    return 0;
  }
  else
    return ((ttldrop / ttlconnect) * 100);
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      start_timers
*
*  DESCRIPTION
*      Starts all three timers
*  RETURNS
*
*----------------------------------------------------------------------------*/
 /*void start_timers(void)
 {
      t_a.StartTimer();
      t_s.StartTimer();
      t_d.StartTimer();
   
 }*/
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
/* void voicetest::access_watch_start(void)
{

  t_a.Timer();
  ACCESS_TIME = t_a.ShowSeconds();

}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since access_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
/*void voicetest::access_watch_stop(void)
{
 ACCESS_TIME = t_a.ShowSeconds();
 ACCESS_TIME+1;
 t_a.ResetTimer();
}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
/*void voicetest::setup_watch_start(void)
{
      t_s.Timer();
      SETUP_TIME = t_s.ShowSeconds();
}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since setup_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
/*void voicetest::setup_watch_stop(void)
{
  SETUP_TIME = t_s.ShowSeconds();
  SETUP_TIME+1;
  t_s.ResetTimer();
}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
/*void voicetest::duration_watch_start(void)
{
      t_d.Timer();
      DURATION_TIME = t_d.ShowSeconds();
}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since duration_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
/*void voicetest::duration_watch_stop(void)
{
  DURATION_TIME = t_d.ShowSeconds();
  DURATION_TIME+1;
  t_d.ResetTimer();
}*/
/*-----------------------------------------------------------------------------*
*  NAME
*      voice_call_dial
*
*  DESCRIPTION
*       Sets the global variable dial_at with the at command to dial a number
*   Ex: ATD1234567890
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::voice_call_dial(void)
{

  DIAL.concat("ATD");
  DIAL.concat(PHONE_NUMBER);
  DIAL.concat(";\0");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      ISU_RTC_SET
*
*  DESCRIPTION
*        Set the
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::ISU_RTC_SET(void)
{

  RTC.concat("AT+CCLK=\"");
  RTC.concat(UTC_TIME);
  RTC.concat("\"\0");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      clear_data_array
*
*  DESCRIPTION
*        Sets all the values in DATA_ARRAY[] to NULL.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::clear_data_array(void)
{
  DATA_ARRAY.remove(0);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      init_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the header content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::init_logfile_string(void)
{
   clear_data_array();

   DATA_ARRAY.concat("Log File\nTest Start Time: (UTC):  20");
   DATA_ARRAY.concat(UTC_TIME);
   DATA_ARRAY.concat("\n--------------------------------------------\nUTC Time              ");
   DATA_ARRAY.concat("Phone Number  Att#  SVID  BeamID   Access  Access(s)   Setup  Setup(s)  Comp  Duration(s) LGCX  LGCY  LGCZ\n");
}
/*-----------------------------------------------------------------------------*
*  NAME
*      logfile_upload_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the log content of the log
*    file. The input att represents the current att number. 
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::logfile_upload_string(int att)
{
  

  clear_data_array();
  
  DATA_ARRAY.concat("20");
  DATA_ARRAY.concat(UTC_TIME);
  DATA_ARRAY.concat("   ");
  
  DATA_ARRAY.concat(PHONE_NUMBER);
  DATA_ARRAY.concat("   ");
  
  DATA_ARRAY.concat(String(att));
  DATA_ARRAY.concat("           ");

  DATA_ARRAY.concat(SVID);
  DATA_ARRAY.concat("   ");

  DATA_ARRAY.concat(BeamID);
  DATA_ARRAY.concat("   ");

  DATA_ARRAY.concat(ACC);
  DATA_ARRAY.concat("   ");
  DATA_ARRAY.concat(String(ACCESS_TIME,2));
  DATA_ARRAY.concat("           ");

  DATA_ARRAY.concat(SET);
  DATA_ARRAY.concat("   ");
  DATA_ARRAY.concat(String(SETUP_TIME,2));
  DATA_ARRAY.concat("           ");

  DATA_ARRAY.concat(COMP);
  DATA_ARRAY.concat("   ");
  DATA_ARRAY.concat(String(DURATION_TIME,2));
  DATA_ARRAY.concat("           ");

 DATA_ARRAY.concat(LGCX);
 DATA_ARRAY.concat("           ");
 DATA_ARRAY.concat(LGCY);
 DATA_ARRAY.concat("           ");
 DATA_ARRAY.concat(LGCZ);
 DATA_ARRAY.concat("           ");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      end_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the summary content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::end_logfile_string(void)
{
  
  

  clear_data_array();
  
  DATA_ARRAY.concat("\n\n\n\n\n\nTest Stop:    20");
  DATA_ARRAY.concat(UTC_TIME);
  DATA_ARRAY.concat("\n-----------------------\n");

  DATA_ARRAY.concat("Attempts      =    ");
  DATA_ARRAY.concat(String(ATTEMPTS));
  DATA_ARRAY.concat("\n");
  
  DATA_ARRAY.concat("Setups        =    ");
  DATA_ARRAY.concat(String(SETUPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Setup Pct     =  ");
  DATA_ARRAY.concat(String(SETUPS_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Connects      =    ");
  DATA_ARRAY.concat(String(CONNECTS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Connect Pct   =  ");
  DATA_ARRAY.concat(String(CONNECT_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Drops         =    ");
  DATA_ARRAY.concat(String(DROPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Drop Pct      =  ");
  DATA_ARRAY.concat(String(DROPS_PCT,2));
  DATA_ARRAY.concat("%\n");
  DATA_ARRAY.concat("\n");
  
}
