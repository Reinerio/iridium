
uint8_t elapsed_second_s = 55, // 0-59
        elapsed_minute_s = 23, // 0-59
        elapsed_hour_s = 14,   // 0-23
        elapsed_day_s = 11,    // 1-31
        elapsed_month_s = 5,  // 1-12
        elapsed_year_s = 2014;   // 0-99 (representing 2000-2099)

static unsigned short days[4][12] =
{
    {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
    { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
    { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
    {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
};



//uint32_t date_time_to_epoch(void);
void epoch_to_date_time(uint32_t epoch);
//void get_date_time(uint8_t yyy, uint8_t mm, uint8_t dd, uint8_t hh, uint8_t mm, uint8_t ss);


void setup() 
{



  Serial.begin(115200);



   uint32_t epoch = 220925799;
    Serial.print("epoch: ");
  Serial.println(epoch);
  epoch_to_date_time(epoch);  
  
  Serial.print("Year: ");
  Serial.println(elapsed_year_s);
  Serial.print("month: ");
  Serial.println(elapsed_month_s);
  Serial.print("day: ");
  Serial.println(elapsed_day_s);
  Serial.print("hour: ");
  Serial.println(elapsed_hour_s);
  Serial.print("minute: ");
  Serial.println(elapsed_minute_s);
  Serial.print("second: ");
  Serial.println(elapsed_second_s);




   
}

void loop() 
{


     
  delay(2000);
}


//uint32_t date_time_to_epoch()
//{
////    unsigned int second = second_s;  // 0-59
////    unsigned int minute = minute_s;  // 0-59
////    unsigned int hour   = hour_s;    // 0-23
////    unsigned int day    = day_s-1;   // 0-30
////    unsigned int month  = month_s-1; // 0-11
////    unsigned int year   = year_s;    // 0-99
////    return (((year/4*(365*4+1)+days[year%4][month]+day)*24+hour)*60+minute)*60+second;
//}


void epoch_to_date_time(uint32_t epoch)
{
  

    elapsed_second_s = epoch%60;    epoch /= 60;

    elapsed_minute_s = epoch%60;     epoch /= 60;

    elapsed_hour_s   = epoch%24;     epoch /= 24;

    float years = epoch/(365*4+1)*4;
    epoch %= 365*4+1;
   
   
    unsigned int year;
    
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    unsigned int month;
    
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }
   
    elapsed_year_s  = years+year;
    elapsed_month_s = month;
    elapsed_day_s   = epoch-days[year][month]-1;
}


//void get_date_time(uint8_t year_yy, uint8_t month_mm, uint8_t day_dd, uint8_t hour_hh, uint8_t minute_mm, uint8_t second_ss)
//{
//  /*Function Variables Declaration*/ 
//  uint8_t total_s = 0, current_s = 0,
//        total_m = 0, current_m = 0, additional_m = 0,
//        total_h = 0, current_h = 0, additional_h = 0,
//        additional_d = 0;
//  
///*CALCULATE SECONDS*/
//    total_s = 55 + second_ss;
//   //Checks if the addition >= 60s
//   //If so a minute is carried and added.
//  if(total_s >= 60)
//    {
//     additional_m = 1;
//     ss = total_s - 60;
//    }
//      else 
//        {
//          additional_m = 0;
//          ss = total_s;
//         }
// /*END CALCULATE SECONDS*/
// /*CALCULATE MINUTES*/  
//   //This condition will never be meet because minute_s is always <= 36 m
//  // Just in case the 23 is ever increased this condition will be meet.
//
//    if(additional_m == 1)
//    {
//        total_m = minute_s + 23 + 1;
//       
//        if(total_m >= 60) //In this case total_m will be 60
//        {
//           mm = total_m - 60; // mm will equal 0
//           additional_h = 1; // an hour is added
//        }
//        else
//        {
//          mm = total_m;
//          additional_h = 0;
//        }
//             
//    }
//    if(additional_m == 0)
//    {
//
//      total_m = minute_s + 23;
//      
//        if(total_m > 59) //In this case total_m will be 60
//        {
//           mm = total_m - 60; // mm will equal 0
//           additional_h = 0; // an hour is added
//        }
//        else
//        {
//          additional_h = 0;
//          mm = total_m;
//        }
//    }    
// /*END CALCULATE MINUTES*/
// /*CALCULATE HOURS*/
//    if(additional_h == 1)
//    {
//    
//      total_h = hour_s + 15;
//      if(total_h >= 24) //In this case total_m will be 60
//        {
//           hh = total_h - 24; // mm will equal 0
//           additional_d = 1; // an day is added
//        }
//        else
//        {
//          hh = total_h;
//          additional_d = 0;
//        }
//             
//    }
//    if(additional_h == 0)
//    {
//      total_h = hour_s + 14;
//        if(total_h >= 24) //In this case total_m will be 60
//        {
//           hh = total_h - 24; // mm will equal 0
//           additional_d = 0; // an hour is added
//        }
//        else
//        {
//          additional_d = 0;
//          hh = total_h;
//        }
//    } 
//    
// /*END CALCULATE HOURS*/
//}



