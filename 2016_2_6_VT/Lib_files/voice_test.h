/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.h$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H

#include <String.h>
#include <WString.h>
//#include <Chrono.h>
//#include "utility/CountUpDownTimer.h"
//#include "CountUpDownTimer.h"

/*============================================================================*
Private Defines
*============================================================================*/

/*============================================================================*
Public Defines
*============================================================================*/
class voicetest{

 private:

   uint8_t MAX_PHONE_NUM_SIZE = 12, DEFAULT_CALL_TIME = 50, 
           DEFAULT_ATTEMPTS = 1000;
   String DEFAULT_PHONE_NUMBER = "5555";
          
 
  public:

  int ATTEMPTS = 0, SETUPS = 0, CONNECTS = 0, DROPS = 0, delay_between_calls = 0;
  
  float   CONNECT_PCT = 0,
          DROPS_PCT = 0,
          SETUPS_PCT = 0,
          ACCESS_TIME = 0,
          DURATION_TIME = 0,
          SETUP_TIME = 0,
          CALL_TIME = 0;
    
   String UTC_TIME = ""/*String value of the Time in the format of yyyy/mm/dd hh:mm:ss*/,
      TEST_STOP_UTC_TIME = ""/*String value of the Time in the format of yyyymmdd_hhmmss*/,
      NETW_STATUS_BYPASS = ""/*String value for Network status used to bypass, checking for the network status.*/,
      LFRAME = ""/*Value of the epoch time returned by IRIDIUM Network.*/,
      PHONE_NUMBER = ""/*String value of the phone number*/,
      BeamID = ""/*String value of the BeamID*/,
      SVID = ""/*String value of the SVID*/,
      ACC = ""/*String value representing a call access.*/,
      COMP = ""/*String value representing a call completion. */,
      SET = ""/*String value representing a call has been set.*/,
      LGCX = ""/*String value for the x-coordinates.*/,
      LGCY = ""/*String value for the y-coordinates.*/,
      LGCZ = ""/*String value for the z-coordinates.*/,
      DIAL = ""/*String value with the AT command to dial and the phone number.*/,
      DATA_ARRAY = ""/*String used to send DATA to the serial monitor.*/,
      DATA_ARRAY_TWO = ""/*String used to send DATA to the serial monitor.*/,
      NetwStatus = ""/*Network Status indicator*/;

      const uint16_t days[4][12] =
                              {
                                  {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
                                  { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
                                  { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
                                  {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
                              }/*Variable used to calculate the day*/;     
      
        uint32_t epoch = 0;//Variable used to represent the epoch value returned threw the ISU

      
        float elapsed_second_s = 0, // 0-59
                elapsed_minute_s = 0, // 0-59
                elapsed_hour_s = 0,   // 0-23
                elapsed_day_s = 0,    // 1-31
                elapsed_month_s = 0,  // 1-12
                elapsed_year_s = 0;   // 00-99 (representing 2000-2099)
                
        /*Uint_8 values used for comparison in the get_date_time function*/
        uint8_t y = 0,
                mo = 0,
                d = 0,
                h = 0,
                m = 0,
                s = 0;
      /*Iridium System Time 2014/5/11 14:23:55*/
      uint8_t IR_SYS_T_Y = 14,
              IR_SYS_T_MO = 5,
              IR_SYS_T_D = 11,
              IR_SYS_T_H = 14,
              IR_SYS_T_M = 23,
              IR_SYS_T_S = 55;   
              
  /*============================================================================*
  Public Functions
  *============================================================================*/
  bool input_phone_number(String phonenum);
  bool input_call_time(float calltime);
  
  bool input_attempts_number(int attemptsnum);
  float calc_setup_pct(float ttlsetups, float ttlattemps);
  float calc_connect_pct(float ttlconnect, float ttlattemps);
  float calc_drop_pct(float ttldrop, float ttlconnect);


  void voice_call_dial(void);
 
  void clear_data_array(void);
  void init_logfile_string(void);
  void end_logfile_string(void);
  void logfile_upload_string(int att);
  
  void epoch_to_date_time(uint32_t epoch);
  void get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss);
  void set_UTC(double epoch_val);
};

#endif /* _VOICE_TEST_H*/ 



