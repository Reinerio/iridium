#include <voice_test.h>
#include<CountUpDownTimer.h>
#include <SoftwareSerial.h>

//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
SoftwareSerial DF_Port(10,6); //SoftwareSerial DF_Port(RX, TX);
SoftwareSerial Dummy_Port(15,14); //Dummy SoftwareSerial DF_Port(RX, TX);

CountUpDownTimer t_a(UP),t_s(UP),t_d(UP);
voicetest vt;

int dial_fail_attempts = 5, time_attempt = 0;
String DF_RX_buffer = "", netw_status_bypass = "";

void get_UTC(char timer);
void get_CIER_DATA(void);
void get_network_satus(void);
void init_file_board(void);

void setup()
{
    Serial.begin(115200);
    while (!Serial) {
        ; // wait for serial port to connect. Needed for native USB
    }
    // set the data rate for the SoftwareSerial port
    DF_Port.begin(57600); //Originaly 115200 since port baud is 115200 slowed down less to no trash data on serial monitor
    DF_Port.listen();
    
    vt.input_phone_number("5555");
    vt.input_call_time(15);
    vt.voice_call_dial();
    vt.input_attempts_number(15);
    /*If set to YES, network status checking will be bypassed*/
    //netw_status_bypass.concat("YES");
    
}

void loop()
{
    
    int attempt_number = 0;
    
    init_file_board(); /*Attempt to obtain network availability, if there
    is a netwrok connection the epoch time will be retrived,
    and a log file header will be created*/
    
    /*LOOP that will execute for # of attempts and generate the logs*/
    for(int i=0;i<vt.ATTEMPTS;i++)
    {
        int dial_attempt = 0,
        time_attempt = 0;
        
        t_a.StartTimer(); /*Access time timer*/
        t_s.StartTimer(); /*Setup time timer*/
        t_d.StartTimer(); /*Druration time timer*/
        
        do
        {
            
            get_network_satus();
            
            if(vt.NetwStatus.indexOf("1") >= 0)
            {
                
                get_UTC('S'); //S
                
                if(vt.UTC_TIME.indexOf("NO") >= 0)
                {
                    // Serial.print("Connection not established file setup = N");
                    vt.SET.concat("N");
                    vt.NetwStatus.remove(0);
                    vt.NetwStatus.concat("OK");
                }
                else
                {
                    //Serial.print("Connection established file setup = Y");
                    vt.SET.concat("Y");
                    vt.SETUPS++;
                    vt.NetwStatus.remove(0);
                    vt.NetwStatus.concat("OK");
                }
                //Clear the 'OK' from the buffer
                DF_RX_buffer.remove(0);
                //Get the seconds it took to set-up
                vt.SETUP_TIME = (t_s.ShowMilliSeconds()/1000) % 3600;
                t_s.StopTimer();  // Stop the timer set-up
            }
            
            else
            {
                time_attempt++;
                delay(1000);
                if(time_attempt >= 5)
                {
                    vt.SET.concat("N");
                    vt.NetwStatus.remove(0);
                    vt.NetwStatus.concat("OK");
                }
                
            }
            
            delay(500);
            
        }while(vt.NetwStatus != "OK");
        
        time_attempt = 0;
        vt.NetwStatus.remove(0);
        
        do
        {
            get_network_satus();
            
            if(vt.NetwStatus.indexOf("1") >= 0)
            {
                
                do /*DIAL the phone number*/
                {
                    
                    DF_Port.print(vt.DIAL); //Send Dial command
                    t_a.Timer(); //Start the Access timer
                    
                    while (DF_Port.available()>0)
                    {
                        DF_RX_buffer = DF_Port.readString();
                    }
                    
                    
                    if(DF_RX_buffer.indexOf("OK") >= 0)
                    {
                        vt.ACC.concat("Y");
                        vt.CONNECTS++;
                        vt.NetwStatus.remove(0);
                        vt.NetwStatus.concat("OK");
                        DF_RX_buffer.remove(0);
                        DF_RX_buffer.concat("OK");
                    }
                    
                    if(DF_RX_buffer.indexOf("NO") >= 0)
                    {
                        //"Recived NO CARRIER"
                        DF_RX_buffer.remove(0);
                        delay(1000);
                        dial_attempt++;
                        if(dial_attempt >= dial_fail_attempts) //try 5 times equals 5 seconds
                        {
                            vt.ACC.concat("N");
                            vt.DROPS++;
                            //Exit no more attempts 'NO CARRIER'
                            DF_RX_buffer.remove(0);
                            DF_RX_buffer.concat("OK");
                            vt.NetwStatus.remove(0);
                            vt.NetwStatus.concat("OK");
                            
                        }
                    }
                    
                    
                    delay(300);
                    
                }while(DF_RX_buffer != "OK"); /*END of DIAL # loop*/
            }
            
            else
            {
                time_attempt++;
                delay(1000);
                if(time_attempt >= 5)
                {
                    vt.ACC.concat("N");
                    vt.DROPS++;
                    vt.NetwStatus.remove(0);
                    vt.NetwStatus.concat("OK");
                }
            }
            
            delay(300);
            
        }while(vt.NetwStatus != "OK");
        
        time_attempt = 0;
        vt.NetwStatus.remove(0);
        
        DF_RX_buffer.remove(0);
        dial_attempt = 0;
        
        vt.ACCESS_TIME = (t_a.ShowMilliSeconds()/1000) % 3600;
        t_a.StopTimer();
        
        if(vt.ACC.indexOf("Y") >= 0)
        {
            do
            {
                t_d.Timer();
                vt.DURATION_TIME = (t_d.ShowMilliSeconds()/1000) % 3600;
                
            }while(vt.DURATION_TIME <= vt.CALL_TIME);
            /* Dependinf on the board type if(9523) BEAMID SVID, X, Y, Z = 0;
            *                             if (9602) we can get the values threw +CIER
            * In this section of the code we will get the BEAMID SVID, X, Y, and Z
            * values from the DPL PORT.
            */
            get_CIER_DATA();
            
            do{
                //"Hang-up call"
                DF_Port.write("ATH\r");
                t_d.Timer();
                
                while (DF_Port.available() > 0)
                {
                    DF_RX_buffer = DF_Port.readString();
                }
                
                if(DF_RX_buffer.indexOf("OK") >= 0) //if OK is recived then exit loop
                {
                    DF_RX_buffer.remove(0);
                    DF_RX_buffer.concat("OK");
                }
                delay(1000);
            } while(DF_RX_buffer != "OK");
            
            vt.DURATION_TIME = (t_d.ShowMilliSeconds()/1000) % 3600;
            t_d.StopTimer();
            vt.COMP = "Y";
        }
        
        if(vt.ACC.indexOf("N") >= 0)
        {
            vt.COMP.concat("N");
            vt.ACCESS_TIME = 0;
        }
        
        
        DF_RX_buffer.remove(0);
        
        /*Get UTC updated every time you log into file*/
        get_UTC('C');
        
        DF_RX_buffer.remove(0);
        
        vt.logfile_upload_string(attempt_number);
        Serial.print(vt.DATA_ARRAY);
        Serial.print(vt.DATA_ARRAY_TWO);
        
        
        vt.ACCESS_TIME = 0;
        vt.DURATION_TIME = 0;
        vt.SETUP_TIME = 0;
        vt.LFRAME.remove(0);
        vt.BeamID.remove(0);
        vt.SVID.remove(0);
        vt.LGCX.remove(0);
        vt.LGCY.remove(0);
        vt.LGCZ.remove(0);
        vt.ACC.remove(0);
        vt.SET.remove(0);
        vt.COMP.remove(0);
        dial_attempt = 0;
        time_attempt = 0;
        
        attempt_number++;
        
    }//END of FOR LOOP
    
    /*END of File String DATA %*/
    vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
    vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
    vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.ATTEMPTS);
    
    do
    {
        vt.NetwStatus.remove(0);
        DF_RX_buffer.remove(0);
        get_network_satus();
        
        if(vt.NetwStatus.indexOf("1") >= 0)
        {
            DF_RX_buffer.remove(0);
            get_UTC('S'); //S
            
            if(vt.UTC_TIME.indexOf("NO") >= 0)
            {
                vt.UTC_TIME.concat("No Network Service");
            }
            
            vt.NetwStatus.remove(0);
            vt.NetwStatus.concat("OK");
        }
        
        else
        {
            time_attempt++;
            delay(1000);
            if(time_attempt >= 5)
            {
                
                vt.NetwStatus.remove(0);
                vt.NetwStatus.concat("OK");
            }
            
        }
        
        delay(300);
        
    }while(vt.NetwStatus != "OK");
    
    DF_RX_buffer.remove(0);
    
    vt.end_logfile_string();
    Serial.print(vt.DATA_ARRAY);
    
    vt.SETUPS = 0;
    vt.CONNECTS = 0;
    vt.DROPS = 0;
    vt.SETUPS_PCT = 0;
    vt.CONNECT_PCT = 0;
    vt.DROPS_PCT = 0;
    
}//END of MAIN LOOP



void get_UTC(char timer)
{
    
    do
    {
        
        vt.UTC_TIME.remove(0);
        vt.TEST_STOP_UTC_TIME.remove(0);
        //Send AT command
        DF_Port.write("AT-MSSTM\r");
        
        if(timer == 'S')
        {
            t_s.Timer();
        }
        if(timer == 'A')
        {
            t_a.Timer();
        }
        if(timer == 'D')
        {
            t_d.Timer();
        }
        if(timer == 'C')
        {
            boolean t = true;
        }
        
        while (DF_Port.available()>0)
        {
            //Serial.print("Rx recived Data: ");
            DF_RX_buffer = DF_Port.readStringUntil('-');
        }
        
        
        if(DF_RX_buffer.indexOf("no") >= 0)
        {
            //Means that there's no network service
            DF_RX_buffer.remove(0);
            delay(1000);
            time_attempt++; //Try the command 5 times
            if(time_attempt >= dial_fail_attempts)
            {
                vt.UTC_TIME.concat("NO NETWORK SERVICE");
                DF_RX_buffer.remove(0);
                time_attempt = 0;
                DF_RX_buffer.concat("OK");
            }
        }
        else
        {
            if(DF_RX_buffer.indexOf("MSSTM:") >= 0)
            {
                
                String epoch_str = "";
                int epoch_size = 0;
                DF_RX_buffer.remove(0,7); //Will remove the "MSSTM: " leaving the HEX value
                epoch_str.concat(DF_RX_buffer);
                char mostSignificantDigit[epoch_str.length()];
                
                for(int i = 0;i<(epoch_str.length());i++)
                {
                    mostSignificantDigit[i] = epoch_str.charAt(i);
                    
                }
                
                Dummy_Port.println(mostSignificantDigit); //If not printed it wont properly convert.
                vt.epoch = (uint32_t)strtol(mostSignificantDigit, NULL, 16);
                
                vt.set_UTC(vt.epoch);
                
                DF_RX_buffer.remove(0);
                DF_RX_buffer.concat("OK");
                
                
            }
        }
        
        
        //wait 3 s and send data again
        delay(20);
    }while( DF_RX_buffer != "OK");
    
}

void get_CIER_DATA(void)
{
    
       do
        {
            DF_RX_buffer.remove(0);
            vt.NetwStatus.remove(0);
            DF_Port.write("AT+CIER?\r");
            
            while (DF_Port.available()>0)
            {
                DF_RX_buffer = DF_Port.readString();
            }
            
            if(DF_RX_buffer.indexOf("CIER") >= 0)
            {
                if(DF_RX_buffer.indexOf("OK") >= 0)
                {
                    int index = DF_RX_buffer.indexOf(","),
                    index_2 = DF_RX_buffer.indexOf(",",index+1),
                    index_3 = DF_RX_buffer.indexOf(",",index_2+1);
                    
                    if(index_3 >= 0)
                    {
                        //Serial.println("+CIER:0,0,0,0,0");
                         DF_Port.write("AT+CIER=0,0,0,0,1\r");
                                
                                while (DF_Port.available()>0)
                                {
                                    DF_RX_buffer = DF_Port.readString();
                                }
                                
                                if(DF_RX_buffer.indexOf("OK") >= 0)
                                {
                                    DF_RX_buffer.remove(0);
                                    
                                    do
                                    {
                                       
                                        DF_Port.write("AT+CIER=1\r");
                                        
                                        while (DF_Port.available()>0)
                                        {
                                            DF_RX_buffer = DF_Port.readString();
                                        }
                                        //Ex string = +CIEV:3,<sv_id>,<beam_id>,<sv_bm>,<x>,<y>,<z>
                                        //            +CIEV:3,30,43,1,2592,-6524,1388
                                        //                                ||
                                        // VALUES     +CIEV:3,<1-127>,<1-48>,<0-1>,<-7160..+7160>,<-7160..+7160>,<-7160..+7160>
                                        
                                        if(DF_RX_buffer.indexOf("CIEV:3") >= 0)
                                        {
                                            int index = DF_RX_buffer.indexOf(","), //finds the first ',' Position = 8
                                            index_2 = DF_RX_buffer.indexOf(",",index+1), //finds the second ','  Position =  ||
                                            index_3 = DF_RX_buffer.indexOf(",",index_2+1), //finds the third ','
                                            index_4 = DF_RX_buffer.indexOf(",",index_3+1), //finds the fourth ','
                                            index_5 = DF_RX_buffer.indexOf(",",index_4+1), //finds the fith ','
                                            index_6 = DF_RX_buffer.indexOf(",",index_5+1), //finds the six ','
                                            index_7 = DF_RX_buffer.indexOf("-",index_6+2); //dins the '-' on the last coordinate
                                            
                                            vt.SVID.concat(DF_RX_buffer.charAt(index+1)); //
                                            vt.SVID.concat(DF_RX_buffer.charAt(index+2)); //
                                            
                                            if(index_2 == (index+4))
                                            {
                                                vt.SVID.concat(DF_RX_buffer.charAt(index+3)); //
                                            }
                                            
                                            vt.BeamID.concat(DF_RX_buffer.charAt(index_2+1)); //
                                            vt.BeamID.concat(DF_RX_buffer.charAt(index_2+2)); //
                                            
                                            
                                            vt.LGCX.concat(DF_RX_buffer.charAt(index_4+1));
                                            if(index_5 == (index_4+5))
                                            {
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+2));
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+3));
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+4));
                                            }
                                            if(index_5 == (index_4+6))
                                            {
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+2));
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+3));
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+4));
                                                vt.LGCX.concat(DF_RX_buffer.charAt(index_4+5));
                                            }
                                            
                                            
                                            vt.LGCY.concat(DF_RX_buffer.charAt(index_5+1));
                                            if(index_6 == (index_5+5))
                                            {
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+2));
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+3));
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+4));
                                            }
                                            if(index_6 == (index_5+6))
                                            {
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+2));
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+3));
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+4));
                                                vt.LGCY.concat(DF_RX_buffer.charAt(index_5+5));
                                            }
                                            
                                            
                                            vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+1));
                                            
                                            if(index_7 >= 0)
                                            {
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+2));
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+3));
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+4));
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+5));
                                            }
                                            else
                                            {
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+2));
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+3));
                                                vt.LGCZ.concat(DF_RX_buffer.charAt(index_6+4));
                                            }
                                            
                                            DF_RX_buffer.remove(0);
                                            DF_RX_buffer.concat("OK");
                                        }
                                        
                       }while(DF_RX_buffer != "OK");
                      }
                        
                    } /*END of if +CIER:0,0,0,0,0*/
                    
                    if(index_3 < 0)
                    {
                        //Serial.println("+CIER:0,0,0");
                        //For this command 
                          vt.BeamID.concat("0");
                          vt.SVID.concat("0");
                          vt.LGCX.concat(" ");
                          vt.LGCY.concat(" ");
                          vt.LGCZ.concat(" ");
                          DF_RX_buffer.remove(0);
                          DF_RX_buffer.concat("OK");
                    } /*END of if "+CIER:0,0,0"*/
                    
                } /*END of if OK*/
                
            } /*END of if CIER*/
            
            delay(300);
            
        }while(DF_RX_buffer != "OK");
        
        DF_RX_buffer.remove(0);

}


void get_network_satus(void)
{
    //Serial.println("get_network_satus");
    if(netw_status_bypass.indexOf("YES") >= 0)
    {
        vt.NetwStatus.concat("1");
    }
    else
    {
        do
        {
            DF_RX_buffer.remove(0);
            vt.NetwStatus.remove(0);
            DF_Port.write("AT+CIER?\r");
            
            while (DF_Port.available()>0)
            {
                DF_RX_buffer = DF_Port.readString();
            }
            
            if(DF_RX_buffer.indexOf("CIER") >= 0)
            {
                if(DF_RX_buffer.indexOf("OK") >= 0)
                {
                    int index = DF_RX_buffer.indexOf(","),
                    index_2 = DF_RX_buffer.indexOf(",",index+1),
                    index_3 = DF_RX_buffer.indexOf(",",index_2+1);
                    
                    if(index_3 >= 0)
                    {
                        //Serial.println("+CIER:0,0,0,0,0");
                        do
                        {
                            DF_RX_buffer.remove(0);
                            
                            DF_Port.write("AT+CIER=1,0,1,0,0\r");
                            
                            while (DF_Port.available()>0)
                            {
                                DF_RX_buffer = DF_Port.readString();
                            }
                            
                            if(DF_RX_buffer.indexOf("+CIEV:1") >= 0)
                            {
                                
                                int index = DF_RX_buffer.indexOf("+");
                                index_2 = DF_RX_buffer.indexOf("0",index_2);
                                //no netwok
                                if(index_2 >= 0)
                                {
                                    vt.NetwStatus.remove(0);
                                    vt.NetwStatus.concat("0");
                                    DF_RX_buffer.remove(0);
                                    DF_RX_buffer.concat("OK");
                                }
                                else
                                {
                                    //if there is not a 0 there should be a 1
                                    //meaning that there's network connection.
                                    
                                    vt.NetwStatus.remove(0);
                                    vt.NetwStatus.concat("1");
                                    DF_RX_buffer.remove(0);
                                    DF_RX_buffer.concat("OK");
                                }
                                
                            }
                            if(DF_RX_buffer.indexOf("ERROR") >= 0)
                            {
                                do
                                {
                                    DF_RX_buffer.remove(0);
                                    
                                    DF_Port.write("AT+CIER=1,0,1\r");
                                    
                                    while (DF_Port.available()>0)
                                    {
                                        DF_RX_buffer = DF_Port.readString();
                                    }
                                    
                                    if(DF_RX_buffer.indexOf("+CIEV:1") >= 0)
                                    {
                                        
                                        int index = DF_RX_buffer.indexOf("+");
                                        index_2 = DF_RX_buffer.indexOf("0",index_2);
                                        if(index_2 >= 0)
                                        {
                                            //no netwok
                                            vt.NetwStatus.remove(0);
                                            vt.NetwStatus.concat("0");
                                            DF_RX_buffer.remove(0);
                                            DF_RX_buffer.concat("OK");
                                        }
                                        else
                                        {
                                            //if there is not a 0 there should be a 1
                                            //meaning that there's network connection.
                                            vt.NetwStatus.remove(0);
                                            vt.NetwStatus.concat("1");
                                            DF_RX_buffer.remove(0);
                                            DF_RX_buffer.concat("OK");
                                        }
                                        
                                    }
                                    delay(300);
                                }while(DF_RX_buffer != "OK");
                            }
                            
                            delay(300);
                        }while(DF_RX_buffer != "OK");
                        
                    } /*END of if +CIER:0,0,0,0,0*/
                    
                    if(index_3 < 0)
                    {
                        //Serial.println("+CIER:0,0,0");
                        do
                        {
                            DF_RX_buffer.remove(0);
                            
                            DF_Port.write("AT+CIER=1,0,1\r");
                            
                            while (DF_Port.available()>0)
                            {
                                DF_RX_buffer = DF_Port.readString();
                            }
                            
                            if(DF_RX_buffer.indexOf("+CIEV:1") >= 0)
                            {
                                
                                int index = DF_RX_buffer.indexOf("+");
                                index_2 = DF_RX_buffer.indexOf("0",index_2);
                                if(index_2 >= 0)
                                {
                                    //no netwok
                                    vt.NetwStatus.remove(0);
                                    vt.NetwStatus.concat("0");
                                    DF_RX_buffer.remove(0);
                                    DF_RX_buffer.concat("OK");
                                }
                                else
                                {
                                    //if there is not a 0 there should be a 1
                                    //meaning that there's network connection.
                                    vt.NetwStatus.remove(0);
                                    vt.NetwStatus.concat("1");
                                    DF_RX_buffer.remove(0);
                                    DF_RX_buffer.concat("OK");
                                }
                                
                            }
                            if(DF_RX_buffer.indexOf("ERROR") >= 0)
                            {
                                do
                                {
                                    DF_RX_buffer.remove(0);
                                    
                                    DF_Port.write("AT+CIER=1,0,1\r");
                                    
                                    while (DF_Port.available()>0)
                                    {
                                        DF_RX_buffer = DF_Port.readString();
                                    }
                                    
                                    if(DF_RX_buffer.indexOf("+CIEV:1") >= 0)
                                    {
                                        
                                        int index = DF_RX_buffer.indexOf("+");
                                        index_2 = DF_RX_buffer.indexOf("0",index_2);
                                        if(index_2 >= 0)
                                        {
                                            //no netwok
                                            vt.NetwStatus.remove(0);
                                            vt.NetwStatus.concat("0");
                                            DF_RX_buffer.remove(0);
                                            DF_RX_buffer.concat("OK");
                                        }
                                        else
                                        {
                                            //if there is not a 0 there should be a 1
                                            //meaning that there's network connection.
                                            vt.NetwStatus.remove(0);
                                            vt.NetwStatus.concat("1");
                                            DF_RX_buffer.remove(0);
                                            DF_RX_buffer.concat("OK");
                                        }
                                        
                                    }
                                    delay(300);
                                }while(DF_RX_buffer != "OK");
                            }
                            
                            delay(300);
                        }while(DF_RX_buffer != "OK");
                        
                    } /*END of if "+CIER:0,0,0"*/
                    
                } /*END of if OK*/
                
            } /*END of if CIER*/
            
            delay(300);
            
        }while(DF_RX_buffer != "OK");
        DF_RX_buffer.remove(0);
    }
}

void init_file_board(void)
{
    do
    {
        
        get_network_satus();
        
        if(vt.NetwStatus.indexOf("1") >= 0)
        {
            
            get_UTC('S'); //S
            
            if(vt.UTC_TIME.indexOf("NO") >= 0)
            {
                vt.UTC_TIME.concat("FAIL NO NETWORK");
            }
            vt.NetwStatus.remove(0);
            vt.NetwStatus.concat("OK");
        }
        
        else
        {
            time_attempt++;
            delay(1000);
            if(time_attempt >= 5)
            {
                
                vt.NetwStatus.remove(0);
                vt.NetwStatus.concat("OK");
            }
            vt.NetwStatus.remove(0);
        }
        
        delay(500);
        
    }while(vt.NetwStatus != "OK");
    
    DF_RX_buffer.remove(0);
    /*Initialize the log file Header*/
    vt.init_logfile_string();
    /*DATA_ARRAY & DATA_ARRAY_TWO contain the header of the log_file*/
    Serial.print(vt.DATA_ARRAY);
    Serial.print(vt.DATA_ARRAY_TWO);
}
