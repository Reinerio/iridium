/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.c$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/
/*============================================================================*
Private Defines
*============================================================================*/
/*============================================================================*
Public Defines
*============================================================================*/
/*============================================================================*
 Public Functions
*============================================================================*/

/*-----------------------------------------------------------------------------*
*  NAME
*      input_phone_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool input_phone_number(char phonenum[])
{
	int ret, max_len, len, i;
	char PHONE_NUMBER_dumy[sizeof(MAX_PHONE_NUM_SIZE) + 3] = "";

	i = 0;
	while (phonenum[i] != '\0') {
		PHONE_NUMBER_dumy[i] = phonenum[i];
		i++;
	}

	PHONE_NUMBER_dumy[i] = '\0';

	strcpy_s(PHONE_NUMBER, PHONE_NUMBER_dumy); /*Copies the string phonenum to PHONE_NUMBER*/

	ret = strcmp(PHONE_NUMBER, DEFAULT_PHONE_NUMBER);

	if (ret == 0) /*DEFAULT_PHONE_NUMBER equals PHONE_NUMBER*/
	{
		return true;
	}
	
	else

    max_len = strlen(MAX_PHONE_NUM_SIZE); /*Gets the lenght of the string*/
	len = strlen(PHONE_NUMBER);

	if (len <= max_len) /*PHONE_NUMBER is less than MAX_PHONE_NUM_SIZE*/
	{
		return true;
	}
	if (len > max_len) /*MAX_PHONE_NUM_SIZE is less than PHONE_NUMBER*/
	{
		return false;
	}
	//else /*MAX_PHONE_NUM_SIZE equals PHONE_NUMBER*/
		//return true;
}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_call_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool input_call_time(float calltime)
{
	CALL_TIME = calltime;

	if (CALL_TIME == 0 || CALL_TIME == DEFAULT_CALL_TIME)
	{
		CALL_TIME = DEFAULT_CALL_TIME;
		return true;
	}
	if (CALL_TIME > MAX_CALL_TIME_SIZE)
	{
		return false;
	}
	else
		/*CALL_TIME = calltime;*/
		return true;
}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_utc_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool input_utc_time(char utctime[])
{
	int default_len, len,i;
	
	char UTC_TIME_dummy[sizeof(UTC_TIME_DEFAULT) + 3] = "";
	i = 0;
	while (utctime[i] != '\0') {
		UTC_TIME_dummy[i] = utctime[i];
		i++;
	}

	UTC_TIME_dummy[i] = '\0';
	UTC_TIME[sizeof(UTC_TIME_dummy)];

	strcpy_s(UTC_TIME, UTC_TIME_dummy); /*Copies the string utctime to UTC_TIME*/

	default_len = strlen(UTC_TIME_DEFAULT); /*Gets the lenght of the string*/
	len = strlen(UTC_TIME);

	if (len == default_len)
	{
		return true;
	}
	else
		return false;


}

/*-----------------------------------------------------------------------------*
*  NAME
*      input_attempts_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool input_attempts_number(float attemptsnum)
{
	ATTEMPTS = attemptsnum;

	if (ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
	{
		ATTEMPTS = DEFAULT_ATTEMPTS;
		return true;
	}
	if (ATTEMPTS > DEFAULT_ATTEMPTS)
	{
		return false;
	}
	else
		/*ATTEMPTS = attemptsnum;*/
		return true;
}

/*-----------------------------------------------------------------------------*
*  NAME
*      calc_setup_pct
*
*  DESCRIPTION
*      Calulates the % of setupts
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_setup_pct(float ttlsetups, float ttlattemps)
{
	return ((ttlsetups / ttlattemps) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_connect_pct
*
*  DESCRIPTION
*      Calulates the % of connects
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_connect_pct(float ttlconnect, float ttlattemps)
{
	return ((ttlconnect / ttlattemps) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_drop_pct
*
*  DESCRIPTION
*      Calulates the % of drops
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_drop_pct(float ttldrop, float ttlconnect)
{
	return ((ttldrop / ttlconnect) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void access_watch_start(void)
{

	time(&start_t_access);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since access_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int access_watch_stop(void)
{
	time(&end_t_access);
	diff_t_access = difftime(end_t_access, start_t_access);

	return (diff_t_access);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void setup_watch_start(void)
{

	time(&start_t_setup);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since setup_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int setup_watch_stop(void)
{
	time(&end_t_setup);
	diff_t_setup = difftime(end_t_setup, start_t_setup);

	return (diff_t_setup);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void duration_watch_start(void)
{

	time(&start_t_duration);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since duration_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int duration_watch_stop(void)
{
	time(&end_t_duration);
	diff_t_duration = difftime(end_t_duration, start_t_duration);

	return (diff_t_duration);
}

/*-----------------------------------------------------------------------------*
*  NAME
*      voice_call_dial
*
*  DESCRIPTION
*       Sets the global variable dial_at with the at command to dial a number
*		Ex: ATD1234567890
*  RETURNS
*      
*----------------------------------------------------------------------------*/
void voice_call_dial(char *s1)
{
	int i;
	char *result = (char*)malloc(strlen("ATD") + strlen(s1) + 1);//+1 for the zero-terminator
	//in real code you would check for errors in malloc here
	strcpy(result, "ATD");
	strcat(result, s1);
	
	char msg_dummy[sizeof(s1) + 5] = "";
	i = 0;
	while (result[i] != '\0') {
		msg_dummy[i] = result[i];
		i++;
	}

	msg_dummy[i] = '\0';
	dial_at[sizeof(msg_dummy)];

	strcpy_s(dial_at, msg_dummy); /*Copies the string utctime to UTC_TIME*/
	
}