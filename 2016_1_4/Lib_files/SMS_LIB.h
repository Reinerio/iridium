/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2016
*
*    FILE
*      $Workfile: SMS_LIB.h$
*      $Revision: 1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the SMS_LIB.c file
*
******************************************************************************/
#ifndef _SMS_LIB_H
#define _SMS_LIB_H

#include <String.h>
#include <WString.h>
/*============================================================================*
Private Defines
*============================================================================*/

/*============================================================================*
Public Defines
*============================================================================*/
#define DEFAULT_CALL_TIME  50
#define DEFAULT_ATTEMPTS   1000

class SMS_LIB{

 private:

   String MAX_PHONE_NUM_SIZE = "9999999999",
          DEFAULT_SMSC = "5555",
          DEFAULT_RECEIVER = "1234567",
          DEFAULT_MSG = "Hello World!";
         
 
  public:

  int ATTEMPTS = 0, SETUPS = 0, CONNECTS = 0, DROPS = 0;
  
  float   CONNECT_PCT = 0,
          DROPS_PCT = 0,
          SETUPS_PCT = 0,
          ACCESS_TIME = 0,
          DURATION_TIME = 0,
          SETUP_TIME = 0,
          CALL_TIME = 0;
    
   String UTC_TIME = "",
      LFRAME = "",
      SMSC_NUMBER = "",
      RECEIVER_NUMBER = "", 
      SMS_MSG = "",
      PDU_MSG = "",
      PDU_MSG_SIZE = "",     
      attempp = "",
      BeamID = "",
      SVID = "",
      ACC = "",
      COMP = "",
      SET = "",
      MODELID = "",
      LGCX = "",
      LGCY = "",
      LGCZ = "",
      ADD_TYPE_AT = "",
      MSG_FORMAT_AT = "",
      SMSC_ADD_AT = "",
      MSG_SEND_AT = "",
      DATA_ARRAY = "",
      DATA_ARRAY_TWO = "",
      NetwStatus = "";

      const uint16_t days[4][12] =
                              {
                                  {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
                                  { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
                                  { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
                                  {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
                              };     
      
        uint32_t epoch = 0;
      
        float elapsed_second_s = 0, // 0-59
                elapsed_minute_s = 0, // 0-59
                elapsed_hour_s = 0,   // 0-23
                elapsed_day_s = 0,    // 1-31
                elapsed_month_s = 0,  // 1-12
                elapsed_year_s = 0;   // 00-99 (representing 2000-2099)
        
        uint8_t y = 0,
                mo = 0,
                d = 0,
                h = 0,
                m = 0,
                s = 0;
      /*Iridium System Time 2014/5/11 14:23:55*/
      uint8_t IR_SYS_T_Y = 14,
              IR_SYS_T_MO = 5,
              IR_SYS_T_D = 11,
              IR_SYS_T_H = 14,
              IR_SYS_T_M = 23,
              IR_SYS_T_S = 55;   
  /*============================================================================*
  Public Functions
  *============================================================================*/
  bool set_SMSC(String phonenum);
  bool set_Reciever(String phonenum);
  bool set_MSG(String MSG);
  
  bool input_attempts_number(int attemptsnum);
  float calc_setup_pct(float ttlsetups, float ttlattemps);
  float calc_connect_pct(float ttlconnect, float ttlattemps);
  float calc_drop_pct(float ttldrop, float ttlconnect);


  void SMS_commands_setup(void);
  void PDU_MSG_generate(void);
  void PDU_MSG_send(void);
 
  void clear_data_array(void);
  void init_logfile_string(void);
  void end_logfile_string(void);
  void logfile_upload_string(int att);
  
  void epoch_to_date_time(uint32_t epoch);
  void get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss);
  void set_UTC(double epoch_val);
};

#endif /* _SMS_LIB_H*/ 



