#include <voice_test.h>
#include<CountUpDownTimer.h>
#include <SoftwareSerial.h>

//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
SoftwareSerial DF_Port(7,6); //SoftwareSerial DF_Port(RX, TX);
CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 
voicetest vt;

int tries = 0, dial_fail_attempts = 3;
String DF_RX_buffer = "",
       RTC = "";
//       str1 = "Log File\nTest Start Time  (UTC):  20",
//       str2 = "\n\n\nUTC Time              ",
//       str3 = "LFrame      Phone Number      Att#  SVID  BeamID  ",
//       str4 = "Access  Access     Setup  Setup  Comp  Duration  AbTerm  LGCX   LGCY   LGCZ\n",
//       str5 = "                                                          Init  Init    ",
//       str6 = "          Time(sec)         Time(sec)        (sec)\n";
      
void get_UTC(void);
  
void setup() 
{
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
    // set the data rate for the SoftwareSerial port
  DF_Port.begin(115200);
  DF_Port.listen();

  vt.input_phone_number("12345678");
  vt.input_call_time(10);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(10);
  vt.BeamID = "12";
  vt.SVID = "09";
  vt.ACC = "Y";
  vt.SET = "Y";
  vt.COMP = "Y";
  vt.LGCX = "107.5";
  vt.LGCY = "98.3";
  vt.LGCZ = "104.3";
  vt.LFRAME = "413982614";
  vt.ABTERM = "G";
 
  vt.ISU_RTC_SET();
  vt.voice_call_dial();

//                      do
//                      {
//                        /*Set Time*/
//                        DF_Port.listen(); 
//                        DF_Port.print(vt.RTC); //Same as .write
//                        Serial.println("+CCLK command send");
//                        
//                        if (DF_Port.available()> 0)
//                          {
//                              Serial.print("RX buffer: ");
//                              DF_RX_buffer = DF_Port.readStringUntil('\n');
//                              Serial.println(DF_RX_buffer);
//                    
//                                  if(DF_RX_buffer.substring(0,5) == "ERROR")
//                                  {
//                                       DF_RX_buffer.remove(0);
//                                       Serial.println("Error detected please check UTC time syntax \"yy/mm/dd,hh:mm:ss\"");
//                                  }
//                                  if(DF_RX_buffer.substring(0,2) == "OK")
//                                  {
//                                       DF_RX_buffer.remove(0);
//                                       DF_RX_buffer.concat("OK");                
//                                  }
//                                  else
//                                  {
//                                       DF_RX_buffer.remove(0);
//                                  }
//                          }
//                      }while(DF_RX_buffer != "OK");



}

void loop()
{

  int attempt_number = 0;
 


//do
//{
//
//    
//    //Send AT command
//    DF_Port.write("AT\r"); 
//
//    while (DF_Port.available()>0)
//    {
//     Serial.print("Rx recived Data: ");  
//     DF_RX_buffer = DF_Port.readString();
//
//    }
//    
//    Serial.println(DF_RX_buffer);
//
//              if(DF_RX_buffer.indexOf("OK") >= 0)
//              {
//                   Serial.println("OK");
//                   DF_RX_buffer.remove(0);
//                   DF_RX_buffer.concat("OK");
//              }
//              else
//              {
//                   Serial.println("ERROR");
//                   DF_RX_buffer.remove(0);
//              }
//
// 
//    delay(20);
//
//}while( DF_RX_buffer != "OK");
//
//  DF_RX_buffer.remove(0);

  get_UTC();

  DF_RX_buffer.remove(0);
  
  
  /*Get UTC updated every time you log into file*/
      //Serial.println("Setting up Log file Header......\nLog File Header:\n");
      vt.init_logfile_string();
     // Serial.print(vt.DATA_ARRAY);
      delay(2000);
//      Serial.println("\n Data Array: ");
//      Serial.println(vt.DATA_ARRAY.length());
//      Serial.println("\n str1: ");
//      Serial.println(str1.length());
//      Serial.println("\n str2: ");
//      Serial.println(str2.length());
//      Serial.println("\n str3: ");
//      Serial.println(str3.length());            
//      Serial.println("\n str4: ");
//      Serial.println(str4.length());            
//      Serial.println("\n str5: ");
//      Serial.println(str5.length());            
//      Serial.println("\n str6: ");
//      Serial.println(str6.length());            
      
//      Serial.print("We will attempt to dial the number (");
//      Serial.print(vt.PHONE_NUMBER);
//      Serial.print(") ");
//      Serial.print(vt.ATTEMPTS);
//      Serial.print(" times each call will last ");
//      Serial.print(vt.CALL_TIME);
//      Serial.println(" seconds");
      
  for(int i=0;i<vt.ATTEMPTS;i++)
  { 
       int dial_attempt = 0;
       t_a.StartTimer();
       t_s.StartTimer();
       t_d.StartTimer(); 
       
       t_s.Timer();
       t_a.Timer();
  
        get_UTC();
//       vt.SET = "Y";
//       vt.SETUPS++;
       
   if(DF_RX_buffer == "OK")
     {
//       Serial.println("Connection established file setup");
       vt.SET = "Y";
       vt.SETUPS++;
     }
      else
      {
//        Serial.println("Connection not established file setup");
        vt.SET = "N";
      } 
      
      DF_RX_buffer.remove(0);
 
  vt.SETUP_TIME = t_s.ShowSeconds(); 
  t_s.StopTimer(); 


 
  DF_RX_buffer.remove(0);
  
do
{

    t_d.Timer();
    vt.DURATION_TIME = t_d.ShowSeconds(); 

             // Serial.print("Sending: ");
             // Serial.println(vt.DIAL);
              DF_Port.print(vt.DIAL); //Send Dial command
              
              while (DF_Port.available()>0)
              {
 //              Serial.print("Rx recived Data: ");  
               DF_RX_buffer = DF_Port.readString();
              }
              
             // Serial.print("RX buffer: ");
             // Serial.println(DF_RX_buffer);
              
              if(DF_RX_buffer.indexOf("OK") >= 0) 
              {
//                  Serial.println("Resived OK");
                  vt.ACC = "Y";
                  vt.CONNECTS++;
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");
              }
              if(DF_RX_buffer.indexOf("NO") >= 0)
              {
 //                 Serial.println("Recived NO CARRIER");
                  DF_RX_buffer.remove(0);
                  delay(1000);
                  dial_attempt++;
                  if(dial_attempt >= dial_fail_attempts) //try 10 times equals 10 seconds
                    {
                       vt.ACC = "N";
                       vt.DROPS++; 
  //                     Serial.println("Exit no more attempts 'NO CARRIER'\n\n\n");                              
                       DF_RX_buffer.remove(0);
                       DF_RX_buffer.concat("OK");
                     }
              }
              if(vt.DURATION_TIME >= vt.CALL_TIME)
              {
 //                 Serial.println("Timed out");
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");
              }


vt.DURATION_TIME = t_d.ShowSeconds(); 

delay(1000);
  
}while(DF_RX_buffer != "OK");

                do
                {
                 t_d.Timer();
                 vt.DURATION_TIME = t_d.ShowSeconds(); 
                 //Serial.print("Time: ");
                // Serial.println(vt.DURATION_TIME);
                   /*
                    * In this section of the code we will get the BEAMID SVID, X, Y, Z and frame 
                    * values from the DPL PORT. For testing we will use the pre-defined values
                    * set on the setup() function.
                    */
                                    
                }while(vt.DURATION_TIME <= vt.CALL_TIME);


    
                        vt.DURATION_TIME = t_d.ShowSeconds();  
                        t_d.StopTimer();
                        DF_RX_buffer.remove(0);
  
                        vt.ACCESS_TIME = t_a.ShowSeconds(); 
                        t_a.StopTimer(); 
                        
                        DF_RX_buffer.remove(0);   
                            
                        do{
                             //Serial.println("Hang-up call");
                             DF_Port.write("ATH\r"); //End Voice Call
                             while (DF_Port.available() > 0)
                                   {
                                    DF_RX_buffer = DF_Port.readString();
                                   }
                            // Serial.print("DF_RX_buffer: ");
                            // Serial.println(DF_RX_buffer);
                                      
                             if(DF_RX_buffer.indexOf("OK") >= 0) //if OK is recived then exit loop
                               {
                                 DF_RX_buffer.remove(0);
                                 DF_RX_buffer.concat("OK");
                                }
                                delay(1000);
                           } while(DF_RX_buffer != "OK");

                        
                        if(vt.ACC == "Y")
                          {
                            vt.COMP = "Y"; 
                          }
                        if(vt.ACC == "N")
                          {
                            vt.COMP = "N";      
                          }
                                      
                                      
                                        DF_RX_buffer.remove(0);
                                       
                                      /*Get UTC updated every time you log into file*/
                                        get_UTC();
                                      
                                        DF_RX_buffer.remove(0);

                                       vt.logfile_upload_string(attempt_number);
                                       Serial.print(vt.DATA_ARRAY);
                                       
                                      //T.ResetTimer(); 
                                       vt.ACCESS_TIME = 0;
                                       vt.DURATION_TIME = 0;
                                       vt.SETUP_TIME = 0;
                                    
                                        attempt_number++;

 }//END of FOR LOOP                                               
                                          
                                      /*END of File String DATA %*/           
                                      vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
                                      vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
                                      vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.CONNECTS);
                                      
                                      get_UTC();
                                      
                                      DF_RX_buffer.remove(0);
                                      
                                      vt.end_logfile_string();
                                      Serial.print(vt.DATA_ARRAY);  

}//END of MAIN LOOP



void get_UTC(void)
{
  do
{

    vt.UTC_TIME.remove(0);
    //Send AT command
    DF_Port.write("AT+CCLK?\r"); 

    while (DF_Port.available()>0)
    {
     //Serial.print("Rx recived Data: ");  
     DF_RX_buffer = DF_Port.readStringUntil('+');
     }

     //Serial.println(DF_RX_buffer);

    if(DF_RX_buffer.indexOf("CCLK") >= 0)
    {
        int index = DF_RX_buffer.indexOf("/"), //finds the first '/'
            index_2 = DF_RX_buffer.indexOf(","), //finds the first ','
            index_2_1 = DF_RX_buffer.indexOf(":"), //finds the first ':'
            index_3 = DF_RX_buffer.indexOf(":",index_2_1+1); //finds the second ':'
            
          if(DF_RX_buffer.indexOf("/",(index+1)) == (index+3))
            {// Serial.println("found th second '/'");
           if(index_2 == 13)
            { //Serial.println("found the ','");
           if(DF_RX_buffer.indexOf(":",(index_3+1)) == (index_3+3))
            { 

                  for(int i = 0;i<17;i++)
                  {
                    char mostSignificantDigit = DF_RX_buffer.charAt(i+5);
                    vt.UTC_TIME.concat(mostSignificantDigit); 
                    //Serial.println(mostSignificantDigit);
                  }     
                  //Serial.println("Time:  ");
                  //Serial.println(vt.UTC_TIME); 
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");              
            
            }
            } 
            }
    
       }
         
   //wait 3 s and send data again 
    delay(20);
}while( DF_RX_buffer != "OK");

//  DF_RX_buffer.remove(0);
  
}













