#include <CountUpDownTimer.h>

#include <voice_test.h>


#include <SoftwareSerial.h>
String DF_RX_buffer = "";
String dumDF_RX_buffer = " ";
String dummy_buff = "\n\n\nrevvgfbfKO";
//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
int tries = 0, dial_fail_attempts = 2;
SoftwareSerial DF_Port(7,6); //SoftwareSerial DF_Port(RX, TX);

CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 
voicetest vt;

void setup() {
  
  // initialize serial:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DF_Port.begin(115200);
  DF_Port.listen(); //Enables the port to listen 




  vt.input_phone_number("12345678");
  vt.input_call_time(20);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(5);
  vt.BeamID = "12";
  vt.SVID = "09";
  vt.ACC = "Y";
  vt.SET = "Y";
  vt.COMP = "Y";
  vt.LGCX = "107.5";
  vt.LGCY = "98.3";
  vt.LGCZ = "104.3";

  vt.ISU_RTC_SET();
  vt.voice_call_dial();

}

void loop() {

 //t_a.StartTimer();
 // t_s.StartTimer();
  t_d.StartTimer(); 
   
     int attempt_number = 0;
     int dial_attempt = 0;
do
{


    //Send AT command
    DF_Port.write("AT\r"); 

    while (DF_Port.available()>0)
    {
    Serial.print("Rx recived Data: ");  
     DF_RX_buffer = DF_Port.readString();

    }
    
    Serial.println(DF_RX_buffer);


              if(DF_RX_buffer.indexOf("OK") >= 0)
              {
                   Serial.println("OK");
                   DF_RX_buffer.remove(0);
                   DF_RX_buffer.concat("OK");
              }
              else
              {
                   Serial.println("ERROR");
                   DF_RX_buffer.remove(0);
              }

 
    delay(20);

}while( DF_RX_buffer != "OK");

 DF_RX_buffer.remove(0);

do
{


    //Send AT command
    DF_Port.write("AT+CCLK?\r"); 

    while (DF_Port.available()>0)
    {
     Serial.print("Rx recived Data: ");  
     DF_RX_buffer = DF_Port.readStringUntil('+');
     }

     Serial.println(DF_RX_buffer);

    if(DF_RX_buffer.indexOf("CCLK") >= 0)
    {
        int index = DF_RX_buffer.indexOf("/"), //finds the first '/'
            index_2 = DF_RX_buffer.indexOf(","), //finds the first ','
            index_2_1 = DF_RX_buffer.indexOf(":"), //finds the first ':'
            index_3 = DF_RX_buffer.indexOf(":",index_2_1+1); //finds the second ':'
            
          if(DF_RX_buffer.indexOf("/",(index+1)) == (index+3))
            {// Serial.println("found th second '/'");
           if(index_2 == 13)
            { //Serial.println("found the ','");
           if(DF_RX_buffer.indexOf(":",(index_3+1)) == (index_3+3))
            { 

                  for(int i = 0;i<17;i++)
                  {
                    char mostSignificantDigit = DF_RX_buffer.charAt(i+5);
                    vt.UTC_TIME.concat(mostSignificantDigit); 
                    //Serial.println(mostSignificantDigit);
                  }     
                  Serial.println("Time:  ");
                  Serial.println(vt.UTC_TIME); 
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");              
            
            }
            } 
            }
    
       }
         
   //wait 3 s and send data again 
    delay(20);
}while( DF_RX_buffer != "OK");

  vt.UTC_TIME.remove(0);
 // t_d.Timer();
                        Serial.println("Call Time: ");
                        Serial.println(vt.CALL_TIME); 
                        Serial.println("Duration Time: ");
                        Serial.println(vt.DURATION_TIME);    






DF_RX_buffer.remove(0);

do
{

t_d.Timer();
vt.DURATION_TIME = t_d.ShowSeconds(); 

              Serial.print("Sending: ");
              Serial.println(vt.DIAL);
              DF_Port.print(vt.DIAL); //Send Dial command
              
              while (DF_Port.available()>0)
              {
               Serial.print("Rx recived Data: ");  
               DF_RX_buffer = DF_Port.readString();
              }
              
              //Serial.print("RX buffer: ");
              Serial.println(DF_RX_buffer);
              
              if(DF_RX_buffer.indexOf("OK") >= 0) 
              {
                  Serial.println("Resived OK");
                  DF_RX_buffer.remove(0);
                  //DF_RX_buffer.concat("OK");
              }
              if(DF_RX_buffer.indexOf("NO") >= 0)
              {
                  Serial.println("Recived NO CARRIER");
                  DF_RX_buffer.remove(0);
                  delay(1000);
                  dial_attempt++;
                             if(dial_attempt >= dial_fail_attempts) //try 10 times equals 10 seconds
                               {
                               Serial.println("Exit no more attempts 'NO CARRIER'\n\n\n");                              
                               DF_RX_buffer.remove(0);
                               DF_RX_buffer.concat("OK");
                               }
              }
              if(vt.DURATION_TIME >= vt.CALL_TIME)
              {
                  Serial.println("Timed out");
                  DF_RX_buffer.remove(0);
                  DF_RX_buffer.concat("OK");
              }


vt.DURATION_TIME = t_d.ShowSeconds(); 

delay(1000);  
}while(DF_RX_buffer != "OK");

do
{
 t_d.Timer();
 vt.DURATION_TIME = t_d.ShowSeconds(); 
 Serial.print("Time: ");
 Serial.println(vt.DURATION_TIME);
   /*
    * In this section of the code we will get the BEAMID SVID, X, Y, Z and frame 
    * values from the DPL PORT. For testing we will use the pre-defined values
    * set on the setup() function.
    */
                    
}while(vt.DURATION_TIME <= vt.CALL_TIME);
DF_RX_buffer.remove(0);
                        do{
                             Serial.println("Hang-up call");
                             DF_Port.write("ATH\r"); //End Voice Call
                             while (DF_Port.available() > 0)
                                   {
                                    DF_RX_buffer = DF_Port.readString();
                                   }
                             Serial.print("DF_RX_buffer: ");
                             Serial.println(DF_RX_buffer);
                                      
                             if(DF_RX_buffer.indexOf("OK") >= 0) //if OK is recived then exit loop
                               {
                                 DF_RX_buffer.remove(0);
                                 DF_RX_buffer.concat("OK");
                                }
                                delay(1000);
                           } while(DF_RX_buffer != "OK");
                           

                        vt.DURATION_TIME = 0;
                        dial_attempt = 0;
                        //vt.DURATION_TIME = t_d.ShowSeconds(); 
                        t_d.StopTimer();
                        DF_RX_buffer.remove(0);









}
