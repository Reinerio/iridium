/******************************************************************************
 *  (C) COPYRIGHT Letsky Innovations 2015
 *
 *    FILE
 *      $Workfile: voice_test.c$
 *      $Revision:$
 *      $Author: Reinerio Milanes$
 *
 *  DESCRIPTION
 *      Defines the variables used by the voice_test.c file
 *
 ******************************************************************************/
 
#include "voice_test.h"
/*============================================================================*
    Public Data
 *============================================================================*/

 
/*============================================================================*
    Private Defines
 *============================================================================*/

/*============================================================================*
    Private Data Types
 *============================================================================*/
 
/*============================================================================*
    Private Function Prototypes
 *============================================================================*/
 
/*============================================================================*
    Private Data
 *============================================================================*/
    string MAX_PHONE_NUM_SIZE = "9999999999";
    short MAX_CALL_TIME_SIZE = 32767;
/*============================================================================*
    Public Function Implementations
 *============================================================================*/

/*-----------------------------------------------------------------------------*
 *  NAME
 *      input_phone_number
 *
 *  DESCRIPTION
 *      Checks for the the value of the variable 
 *  RETURNS
 *      True if all is good, false otherwise 
 *----------------------------------------------------------------------------*/
 extern void input_phone_number( string phonenum)
 {
     int ret,retu;
     strcpy(PHONE_NUMBER,phonenum); /*Copies the string phonenum to PHONE_NUMBER*/
     
     ret = strcmp(PHONE_NUMBER, DEFAULT_PHONE_NUMBER);
     
     if(ret = 0) /*DEFAULT_PHONE_NUMBER equals PHONE_NUMBER*/ 
     {
         return true;
     }
     
     else
     
        retu = strcmp(PHONE_NUMBER, MAX_PHONE_NUM_SIZE);
          
        if(retu < 0) /*PHONE_NUMBER is less than MAX_PHONE_NUM_SIZE*/
        {
             return true; 
        }
         else if(retu > 0) /*MAX_PHONE_NUM_SIZE is less than PHONE_NUMBER*/ 
        {
             return false; 
        }
        else /*MAX_PHONE_NUM_SIZE equals PHONE_NUMBER*/ 
             return true; 
 }
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      input_call_time
 *
 *  DESCRIPTION
 *      Checks for the the value of the variable 
 *  RETURNS
 *      True if all is good, false otherwise      
 *----------------------------------------------------------------------------*/
 extern void input_call_time(short calltime)
 {
     CALL_TIME = calltime;
     
     if(CALL_TIME == 0 || CALL_TIME == DEFAULT_CALL_TIME)
     {
         CALL_TIME = DEFAULT_CALL_TIME;
         return true;
     }
     else if(CALL_TIME > MAX_CALL_TIME_SIZE)
     {
         return false;        
     }
     else 
         /*CALL_TIME = calltime;*/  
         return true;      
   }
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      input_utc_time
 *
 *  DESCRIPTION
 *      Checks for the the value of the variable 
 *  RETURNS
 *      True if all is good, false otherwise 
 *      
 *----------------------------------------------------------------------------*/
extern void input_utc_time(string utctime)
{
     int default_len, len;
     strcpy(UTC_TIME,utctime); /*Copies the string utctime to UTC_TIME*/
     
     default_len = strlen(UTC_TIME_DEFAULT); /*Gets the lenght of the string*/    
     len = strlen(UTC_TIME);
     
     if(len == default_len)
     {
        return true;
     }
     else
        return false;
         

}

/*-----------------------------------------------------------------------------*
 *  NAME
 *      input_attempts_number
 *
 *  DESCRIPTION
 *      Checks for the the value of the variable 
 *  RETURNS
 *      True if all is good, false otherwise 
 *      
 *----------------------------------------------------------------------------*/
 extern void input_attempts_number(short attemptsnum)
 {
      ATTEMPTS = attemptsnum;
     
     if(ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
     {
         ATTEMPTS = DEFAULT_ATTEMPTS;
         return true;
     }
     else if(ATTEMPTS > DEFAULT_ATTEMPTS)
     {
         return false;        
     }
     else
        /*ATTEMPTS = attemptsnum;*/  
         return true;  
 }     
 
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      calc_setup_pct
 *
 *  DESCRIPTION
 *      Calulates the % of setupts
 *  RETURNS
 *      persentage  
 *      
 *----------------------------------------------------------------------------*/ 
extern int calc_setup_pct(int ttlsetups, int ttlattemps)
{
    return ((ttlsetups/ttlattemps)*100);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      calc_connect_pct
 *
 *  DESCRIPTION
 *      Calulates the % of connects
 *  RETURNS
 *      persentage  
 *      
 *----------------------------------------------------------------------------*/ 
extern int calc_connect_pct(int ttlconnect, int ttlattemps)
{
     return ((ttlconnect/ttlattemps)*100);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      calc_drop_pct
 *
 *  DESCRIPTION
 *      Calulates the % of drops
 *  RETURNS
 *      persentage  
 *      
 *----------------------------------------------------------------------------*/ 
extern int calc_drop_pct(int ttldrop, int ttlconnect)
{
    return ((ttldrop/ttlconnect)*100);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      access_watch_start
 *
 *  DESCRIPTION
 *      Starts the watch
 *  RETURNS
 *      
 *----------------------------------------------------------------------------*/ 
extern void access_watch_start(void)
{
   
   time(&start_t_access);
   /*Sleeping for 1 seconds*/
   sleep(1);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      access_watch_stop
 *
 *  DESCRIPTION
 *       Returns the difference in seconds since access_watch_start() was called.
 *  RETURNS
 *      Seconds
 *----------------------------------------------------------------------------*/ 
extern int access_watch_stop(void)
{
   time(&end_t_access);
   diff_t_access = difftime(end_t_access, start_t_access);
   
   return (diff_t_access-1);
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      setup_watch_start
 *
 *  DESCRIPTION
 *      Starts the watch
 *  RETURNS
 *      
 *----------------------------------------------------------------------------*/ 
extern void setup_watch_start(void)
{
   
   time(&start_t_setup);
   /*Sleeping for 1 seconds*/
   sleep(1);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      setup_watch_stop
 *
 *  DESCRIPTION
 *       Returns the difference in seconds since setup_watch_start() was called.
 *  RETURNS
 *      Seconds
 *----------------------------------------------------------------------------*/ 
extern int setup_watch_stop(void)
{
   time(&end_t_setup);
   diff_t_setup = difftime(end_t_setup, start_t_setup);
   
   return (diff_t_setup-1);
}
/*-----------------------------------------------------------------------------*
 *  NAME
 *      duration_watch_start
 *
 *  DESCRIPTION
 *      Starts the watch
 *  RETURNS
 *      
 *----------------------------------------------------------------------------*/ 
extern void duration_watch_start(void)
{
   
   time(&start_t_duration);
   /*Sleeping for 1 seconds*/
   sleep(1);
}
 /*-----------------------------------------------------------------------------*
 *  NAME
 *      duration_watch_stop
 *
 *  DESCRIPTION
 *       Returns the difference in seconds since duration_watch_start() was called.
 *  RETURNS
 *      Seconds
 *----------------------------------------------------------------------------*/ 
extern int duration_watch_stop(void)
{
   time(&end_t_duration);
   diff_t_duration = difftime(end_t_duration, start_t_duration);
   
   return (diff_t_duration-1);
}