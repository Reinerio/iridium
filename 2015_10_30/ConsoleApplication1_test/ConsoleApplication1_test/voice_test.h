
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H

#include "windows.h"
#include "winbase.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <cstring>
#include <stdlib.h>

/*============================================================================*
Private Defines
*============================================================================*/
#define TPI_PARAM_IDX (TPI_OPCODE_IDX+TPI_OPCODE_SIZE)
#define TPI_OPCODE_IDX 0
#define TPI_OPCODE_SIZE 1

/*============================================================================*
Public Defines
*============================================================================*/
#define DEFAULT_CALL_TIME	50
#define DEFAULT_ATTEMPTS   1000
#define MAX_CALL_TIME_SIZE 32767
#define TPI_RRCS_SV_ID_BYTE (TPI_PARAM_IDX + 8)
#define TPI_RRCS_BEAM_ID_BYTE (TPI_PARAM_IDX + 9)
#define TPI_RRCS_X_COORD_WORD (TPI_PARAM_IDX + 10)
#define TPI_RRCS_Y_COORD_WORD (TPI_PARAM_IDX + 12)
#define TPI_RRCS_Z_COORD_WORD (TPI_PARAM_IDX + 14) 

#ifdef __cplusplus 
extern "C" {
#endif 
	int ATTEMPTS = 0, SETUPS = 0, CONNECTS = 0, DROPS = 0;

	float
		CONNECT_PCT = 0,
		DROPS_PCT = 0,
		SETUPS_PCT = 0,
		ACCESS_TIME = 0,
		DURATION_TIME = 0,
		SETUP_TIME = 0,
		CALL_TIME = 0;
	
	bool COMPLETE = false, SETUP = false;

	char UTC_TIME_DEFAULT[] = "01/01/01,01:01:01",
		UTC_TIME[18] = "",
		RTC_COMMAND[28] = "",
		PHONE_NUMBER[] = "             ",
		MAX_PHONE_NUM_SIZE[] = "9999999999",
		dial_at[20] = "",
		attempp[5] = "",
		BeamID[8] = "",
		SVID[6] = "",
		ACC[6] = "",
		COMP[6] = "",
		SET[6] = "",
		LGCX[7] = "",
		LGCY[7] = "",
		LGCZ[7] = "";

	const char DEFAULT_PHONE_NUMBER[] = "5555";

	char DATA_ARRAY[255] = "";

	time_t start_t_access, end_t_access,
		start_t_setup, end_t_setup,
		start_t_duration, end_t_duration;

	short diff_t_access,
		diff_t_setup,
		diff_t_duration;

	/*============================================================================*
	Public Functions
	*============================================================================*/
	bool input_phone_number(char phonenum[]);
	bool input_call_time(float calltime);
	bool input_utc_time(char utctime[]);
	bool input_attempts_number(int attemptsnum);
	float calc_setup_pct(float ttlsetups, float ttlattemps);
	float calc_connect_pct(float ttlconnect, float ttlattemps);
	float calc_drop_pct(float ttldrop, float ttlconnect);
	extern void access_watch_start(void);
	extern void setup_watch_start(void);
	extern void duration_watch_start(void);
	extern int access_watch_stop(void);
	extern int setup_watch_stop(void);
	extern int duration_watch_stop(void);
	void voice_call_dial(void);
	void clear_data_array(void);
	void init_logfile_string(void);
	void end_logfile_string(void);
	void logfile_upload_string(void);
	void RTC_set_string(void);

#ifdef __cplusplus 
}
#endif 

#endif /* _VOICE_TEST_H*/ 