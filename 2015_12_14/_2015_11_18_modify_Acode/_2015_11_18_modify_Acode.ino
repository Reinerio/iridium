#include <CountUpDownTimer.h>

#include <SoftwareSerial.h>

String DF_RX_buffer = "";
String dumDF_RX_buffer = "Hello World";
int set_up = 0, access = 0, duration = 0;
//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
int dial_fail_attempts = 5, time_attempt = 0;
uint32_t epoch;
SoftwareSerial DF_Port(10,6); //SoftwareSerial mySerial(RX, TX);
SoftwareSerial Dummy_Port(15,14);
void get_AT_Commands(void);
CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 

void setup() {
  
  // initialize serial:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DF_Port.begin(57600);
  DF_Port.listen();
}

void loop() {
  
get_AT_Commands('S');
//    DF_Port.listen(); //Enables the port to listen 
//    //Send AT command
//   
//       t_a.StartTimer();
//       t_s.StartTimer();
//       t_d.StartTimer(); 
//       
//      
//       Serial.println("Sart Timer"); 
//       
//       get_AT_Commands('S');
//       set_up = (t_s.ShowMilliSeconds()/1000) % 3600; 
//       Serial.print("Set up Time: ");
//       Serial.println(set_up);
//       set_up = 0;
//       t_s.StopTimer(); 
//
//       
//       get_AT_Commands('A');
//       access = (t_a.ShowMilliSeconds()/1000) % 3600;  
//       Serial.print("Access Time: ");
//       Serial.println(access); 
//       access = 0;
//       t_a.StopTimer(); 
//       
//       
//          
//       get_AT_Commands('D');
//       duration = (t_d.ShowMilliSeconds()/1000) % 3600;  
//       Serial.print("Duration Time: ");
//       Serial.println(duration);     
//       duration = 0;     
//       t_d.StopTimer();


          
}

void get_AT_Commands(char timer)
{
   
   do
   {
   DF_Port.write("AT-MSSTM\r"); 

     if(timer == 'S')
     {
      t_s.Timer();
     }
     if(timer == 'A')
     {
      t_a.Timer();
     }
     if(timer == 'D')
     {
      t_d.Timer();
     }


    //If > 0 means that there's data on the buffer
    while (DF_Port.available()>0)
    {
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
     
     DF_RX_buffer = DF_Port.readStringUntil('-');
    }
    Serial.print("Rx recived Data: ");  
    Serial.println(DF_RX_buffer);
    
    if(DF_RX_buffer.indexOf("no") >= 0)
    {
       //Means that there's no network service
                  DF_RX_buffer.remove(0);
                  delay(1000);
                  time_attempt++; //Try the command 5 times
                  if(time_attempt >= dial_fail_attempts) 
                    {
                       Serial.println("NO NETWORK SERVICE"); 
                       DF_RX_buffer.remove(0);
                       time_attempt = 0;
                       DF_RX_buffer.concat("OK");
                     }
    }
    else
    {
      Serial.println("Enteer else"); 
     if(DF_RX_buffer.indexOf("MSSTM:") >= 0)
    {
          
        String epoch_str = "";
        int epoch_size = 0;
        DF_RX_buffer.remove(0,7); //Will remove the "MSSTM: " leaving the HEX value
        epoch_str.concat(DF_RX_buffer);
        char mostSignificantDigit[epoch_str.length()];
        
        for(int i = 0;i<(epoch_str.length());i++)
        {
           mostSignificantDigit[i] = epoch_str.charAt(i);
        }

        Dummy_Port.println(mostSignificantDigit);
        epoch = (uint32_t)strtol(mostSignificantDigit, NULL, 16); 

        Serial.print(epoch);
            
    }
    }

    DF_RX_buffer.remove(0);

    //wait 3 s and send data again 
    delay(20);
   }while(DF_RX_buffer != "OK");
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
   DF_RX_buffer.remove(0);
   
}

