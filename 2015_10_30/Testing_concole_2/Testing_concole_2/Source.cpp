/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.c$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/
#include "voice_test.h"
#include <vector>

using namespace std;


int main()
{
	bool condition = false;
	int x;

	char str2[10] = "";
	char mystring[sizeof(MAX_PHONE_NUM_SIZE) + 3] = "";
	char mystring2[sizeof(UTC_TIME_DEFAULT) + 3] = "";
	cout << "Prog to test basic functions" << endl;
	cout << "Access, Setup, and Durations Timers started" << endl;

	access_watch_start();
	setup_watch_start();
	duration_watch_start();

	while (condition == false)
	{
		cout << "Format: 1234567890 " << endl;
		cout << "ENTER phone number : ";
		gets_s(mystring);
		condition = input_phone_number(mystring);
	}
	voice_call_dial();
	
	condition = false;

	while (condition == false)
	{
		cout << "Format: 00/01/01,24:01:00" << endl;
		cout << "Format: YY/MM/DD,HH:MM:SS" << endl;
		cout << "ENTER UTC Time: ";
		gets_s(mystring2);


		condition = input_utc_time(mystring2);
	}

	condition = false;

	while (condition == false)
	{
		float x;
		cout << "Max value 32767  0 = 50 default time" << endl;
		cout << "ENTER Call Duration: ";
		cin >> x;

		condition = input_call_time(x);
	}

	condition = false;

	while (condition == false)
	{
		float x;
		cout << "Max value 1000  0 = 1000 default attempts" << endl;
		cout << "ENTER # of Attempts: ";
		cin >> x;

		condition = input_attempts_number(x);
	}

	voice_call_dial();
	ISU_RTC_SET();

	SETUPS_PCT = calc_setup_pct(50, 0);
	CONNECT_PCT = calc_connect_pct(14, ATTEMPTS);
	DROPS_PCT = calc_drop_pct(0, 56);

	ACCESS_TIME = access_watch_stop();
	SETUP_TIME = setup_watch_stop();
	DURATION_TIME = duration_watch_stop();
	

	cout << "Resulst:  " << endl;
	cout << "Date: " << UTC_TIME << endl;
	cout << "Phone Number: " << PHONE_NUMBER << endl;
	cout << "Call Duration: " << CALL_TIME << endl;
	cout << "Number of Attempts: " << ATTEMPTS << endl;

	cout << "Setups: " << SETUPS_PCT << "%" << endl;
	cout << "Connects: " << CONNECT_PCT << "%" << endl;
	cout << "Drops: " << DROPS_PCT << "%" << endl;

	cout << "Access Timer: " << ACCESS_TIME << "s" << endl;
	cout << "Setup Timer: " << SETUP_TIME << "s" << endl;
	cout << "Duration Timer: " << DURATION_TIME << "s" << endl;

	init_logfile_string();
	end_logfile_string();
	logfile_upload_string();

	while (true)
	{

	}

	return 0;
}


/*-----------------------------------------------------------------------------*
*  NAME
*      input_phone_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool input_phone_number(char phonenum[])
{


	if (strcmp(phonenum, DEFAULT_PHONE_NUMBER) == 0) /*DEFAULT_PHONE_NUMBER equals PHONE_NUMBER*/
	{
		strcat(PHONE_NUMBER, "5555");
		return true;
	}

	else

	if (strlen(PHONE_NUMBER) <= strlen(MAX_PHONE_NUM_SIZE)) /*PHONE_NUMBER is less than MAX_PHONE_NUM_SIZE*/
	{
		strcat(PHONE_NUMBER, phonenum);
		return true;
	}
	if (strlen(PHONE_NUMBER) > strlen(MAX_PHONE_NUM_SIZE)) /*MAX_PHONE_NUM_SIZE is less than PHONE_NUMBER*/
	{
		return false;
	}

}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_call_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool input_call_time(float calltime)
{
	CALL_TIME = calltime;

	if (CALL_TIME == 0 || CALL_TIME == DEFAULT_CALL_TIME)
	{
		CALL_TIME = DEFAULT_CALL_TIME;
		return true;
	}
	if (CALL_TIME > MAX_CALL_TIME_SIZE)
	{
		return false;
	}
	else
		/*CALL_TIME = calltime;*/
		return true;
}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_utc_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool input_utc_time(char utctime[])
{
	strcat(UTC_TIME, utctime);

	if (strlen(UTC_TIME) == strlen(UTC_TIME_DEFAULT))
	{
		return true;
	}
	else
		return false;


}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_attempts_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool input_attempts_number(int attemptsnum)
{
	ATTEMPTS = attemptsnum;

	if (ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
	{
		ATTEMPTS = DEFAULT_ATTEMPTS;
		return true;
	}
	if (ATTEMPTS > DEFAULT_ATTEMPTS)
	{
		return false;
	}
	else
		/*ATTEMPTS = attemptsnum;*/
		return true;
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_setup_pct
*
*  DESCRIPTION
*      Calulates the % of setupts
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_setup_pct(float ttlsetups, float ttlattemps)
{
	if (ttlsetups == 0 || ttlattemps == 0)
	{
		return 0;
	}
	else
		return ((ttlsetups / ttlattemps) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_connect_pct
*
*  DESCRIPTION
*      Calulates the % of connects
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_connect_pct(float ttlconnect, float ttlattemps)
{
	if (ttlconnect == 0 || ttlattemps == 0)
	{
		return 0;
	}
	else
		return ((ttlconnect / ttlattemps) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_drop_pct
*
*  DESCRIPTION
*      Calulates the % of drops
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float calc_drop_pct(float ttldrop, float ttlconnect)
{
	if (ttldrop == 0 || ttlconnect == 0)
	{
		return 0;
	}
	else
		return ((ttldrop / ttlconnect) * 100);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void access_watch_start(void)
{

	time(&start_t_access);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      access_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since access_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int access_watch_stop(void)
{
	time(&end_t_access);
	diff_t_access = difftime(end_t_access, start_t_access);

	return (diff_t_access);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void setup_watch_start(void)
{

	time(&start_t_setup);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      setup_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since setup_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int setup_watch_stop(void)
{
	time(&end_t_setup);
	diff_t_setup = difftime(end_t_setup, start_t_setup);

	return (diff_t_setup);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_start
*
*  DESCRIPTION
*      Starts the watch
*  RETURNS
*
*----------------------------------------------------------------------------*/
extern void duration_watch_start(void)
{

	time(&start_t_duration);
	/*Sleeping for 1 seconds*/
	//sleep(1);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      duration_watch_stop
*
*  DESCRIPTION
*       Returns the difference in seconds since duration_watch_start() was called.
*  RETURNS
*      Seconds
*----------------------------------------------------------------------------*/
extern int duration_watch_stop(void)
{
	time(&end_t_duration);
	diff_t_duration = difftime(end_t_duration, start_t_duration);

	return (diff_t_duration);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      voice_call_dial
*
*  DESCRIPTION
*       Sets the global variable dial_at with the at command to dial a number
*		Ex: ATD1234567890
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voice_call_dial(void)
{

	strcat(DIAL, "ATD");
	strcat(DIAL, PHONE_NUMBER);
	strcat(DIAL, ";\0");


}
/*-----------------------------------------------------------------------------*
*  NAME
*      ISU_RTC_SET
*
*  DESCRIPTION
*        Set the
*  RETURNS
*
*----------------------------------------------------------------------------*/
void ISU_RTC_SET(void)
{
	strcat(RTC, "AT+CCLK=\"");
	strcat(RTC, UTC_TIME);
	strcat(RTC, "\"\0");
}
/*-----------------------------------------------------------------------------*
*  NAME
*      clear_data_array
*
*  DESCRIPTION
*        Sets all the values in DATA_ARRAY[] to NULL.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void clear_data_array(void)
{
	for (int i = 0; i < sizeof(DATA_ARRAY); i++)
	{
		DATA_ARRAY[i] = NULL;
	}
}
/*-----------------------------------------------------------------------------*
*  NAME
*      init_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the header content of the log
*		 file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void init_logfile_string(void)
{
	clear_data_array();

	strcat(DATA_ARRAY, "Log File\nTest Start Time: (UTC):  20");
	strcat(DATA_ARRAY, UTC_TIME);
	strcat(DATA_ARRAY, "\n--------------------------------------------\nUTC Time              Phone Number  Att#  SVID  BeamID   Access  Access(s)   Setup  Setup(s)  Comp  Duration(s) LGCX  LGCY  LGCZ\n");



}
/*-----------------------------------------------------------------------------*
*  NAME
*      logfile_upload_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the log content of the log
*		 file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void logfile_upload_string(void)
{
	char buffer1[6], buffer2[11], buffer3[11], buffer4[11];

	clear_data_array();
	strcat(DATA_ARRAY, "20");
	strcat(DATA_ARRAY, UTC_TIME);
	strcat(DATA_ARRAY, "   ");
	strcat(DATA_ARRAY, PHONE_NUMBER);
	strcat(DATA_ARRAY, "          ");
	sprintf(attempp, "%d", ATTEMPTS);
	strcat(DATA_ARRAY, attempp);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, SVID);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, BeamID);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, ACC);
	strcat(DATA_ARRAY, "          ");
	sprintf(buffer2, "%4.2f", ACCESS_TIME);
	strcat(DATA_ARRAY, buffer2);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, SET);
	strcat(DATA_ARRAY, "          ");
	sprintf(buffer3, "%4.2f", SETUP_TIME);
	strcat(DATA_ARRAY, buffer3);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, COMP);
	strcat(DATA_ARRAY, "          ");
	sprintf(buffer4, "%4.2f", DURATION_TIME);
	strcat(DATA_ARRAY, buffer4);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, LGCX);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, LGCY);
	strcat(DATA_ARRAY, "          ");
	strcat(DATA_ARRAY, LGCZ);
	strcat(DATA_ARRAY, "          ");

}
/*-----------------------------------------------------------------------------*
*  NAME
*      end_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the summary content of the log
*		 file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void end_logfile_string(void)
{
	char buffer1[11];

	clear_data_array();
	strcat(DATA_ARRAY, "Test Stop:    20");
	strcat(DATA_ARRAY, UTC_TIME);
	strcat(DATA_ARRAY, "\n-----------------------\n");

	strcat(DATA_ARRAY, "Attempts      =    ");
	sprintf(buffer1, "%d", ATTEMPTS);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Setups        =    ");
	sprintf(buffer1, "%d", SETUPS);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Setup Pct     =  ");
	sprintf(buffer1, "%3.2f", SETUPS_PCT);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "%\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Connects      =    ");
	sprintf(buffer1, "%d", CONNECTS);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Connect Pct   =  ");
	sprintf(buffer1, "%3.2f", CONNECT_PCT);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "%\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Drops         =    ");
	sprintf(buffer1, "%d", DROPS);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "\n");

	memset(buffer1, 0, sizeof buffer1);

	strcat(DATA_ARRAY, "Drop Pct      =  ");
	sprintf(buffer1, "%3.2f", DROPS_PCT);
	strcat(DATA_ARRAY, buffer1);
	strcat(DATA_ARRAY, "%\n");
	strcat(DATA_ARRAY, "\n");
}
