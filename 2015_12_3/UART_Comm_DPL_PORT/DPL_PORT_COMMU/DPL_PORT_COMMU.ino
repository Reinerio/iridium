#include <SoftwareSerial.h>
//PIN 4 set to RX << DF_RX
//PIN 5 set to TX >> DF_TX
SoftwareSerial DPL_PORT(4,5); //SoftwareSerial mySerial(RX, TX);

String DPL_RX_buffer = "";

void setup() 
{
   // initialize serial:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DPL_PORT.begin(115200);

}

void loop()
{
  DPL_PORT.listen();
  
  DPL_PORT.write(0xD7); 

  if (DPL_PORT.available()>0)
  {
    DPL_RX_buffer = DPL_PORT.readString();
    Serial.print("Rx recived Data: ");  
    Serial.println(DPL_RX_buffer); 
  }
 
  if (DPL_PORT.available()<=0)
  {
   
    Serial.println("Rx buffer empty");  
  
  }
  delay(3000);
 
}
