/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.cpp$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/

/*============================================================================*
Private Defines
*============================================================================*/
#include "voice_test.h"

/*============================================================================*
Public Defines
*============================================================================*/

/*============================================================================*
 Public Functions
*============================================================================*/


/*-----------------------------------------------------------------------------*
*  NAME
*      input_phone_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool voicetest::input_phone_number(String phonenum)
{

 
  if (phonenum.compareTo(DEFAULT_PHONE_NUMBER) == 0) /*DEFAULT_PHONE_NUMBER equals PHONE_NUMBER*/
  {
    PHONE_NUMBER.concat("5555");
    return true;
  }
  
   else
     
      if (PHONE_NUMBER.length() <= MAX_PHONE_NUM_SIZE.length()) /*PHONE_NUMBER is less than MAX_PHONE_NUM_SIZE*/
          {
            PHONE_NUMBER.concat(phonenum);
            return true;
          }
      if (PHONE_NUMBER.length() > MAX_PHONE_NUM_SIZE.length()) /*MAX_PHONE_NUM_SIZE is less than PHONE_NUMBER*/
          {
           return false;
          }

}
/*-----------------------------------------------------------------------------*
*  NAME
*      input_call_time
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*----------------------------------------------------------------------------*/
bool voicetest::input_call_time(float calltime)
{
  CALL_TIME = calltime;

  if (CALL_TIME == 0 || CALL_TIME == DEFAULT_CALL_TIME)
  {
    CALL_TIME = DEFAULT_CALL_TIME;
    return true;
  }
  if (CALL_TIME > MAX_CALL_TIME_SIZE)
  {
    return false;
  }
  else
   
    return true;
   
}

/*-----------------------------------------------------------------------------*
*  NAME
*      input_attempts_number
*
*  DESCRIPTION
*      Checks for the the value of the variable
*  RETURNS
*      True if all is good, false otherwise
*
*----------------------------------------------------------------------------*/
bool voicetest::input_attempts_number(int attemptsnum)
{
  
  ATTEMPTS = attemptsnum;

  if (ATTEMPTS == 0 || ATTEMPTS == DEFAULT_ATTEMPTS)
  {
    ATTEMPTS = DEFAULT_ATTEMPTS;
    return true;
  }
  if (ATTEMPTS > DEFAULT_ATTEMPTS)
  {
    return false;
  }
  else

    return true;
    
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_setup_pct
*
*  DESCRIPTION
*      Calulates the % of setupts
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_setup_pct(float ttlsetups, float ttlattemps)
{
 
  if (ttlsetups == 0)
  {
    return 0;
  }
  else
    return ((ttlsetups / ttlattemps) * 100);

}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_connect_pct
*
*  DESCRIPTION
*      Calulates the % of connects
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_connect_pct(float ttlconnect, float ttlattemps)
{
  if (ttlconnect == 0)
  {
    return 0;
  }
  else
    return ((ttlconnect / ttlattemps) * 100);
   
}
/*-----------------------------------------------------------------------------*
*  NAME
*      calc_drop_pct
*
*  DESCRIPTION
*      Calulates the % of drops
*  RETURNS
*      persentage
*
*----------------------------------------------------------------------------*/
float voicetest::calc_drop_pct(float ttldrop, float ttlattemps)
{
  if (ttldrop == 0)
  {
    return 0;
  }
  else
    return ((ttldrop / ttlattemps) * 100);
   
}

/*-----------------------------------------------------------------------------*
*  NAME
*      epoch_to_date_time
*
*  DESCRIPTION
*      Innput the number of seconds since the last Iridium epoch date. Sets the 
*      the grloval variables with year, month, day, hour, minute, seconds.
*  RETURNS
*      
*----------------------------------------------------------------------------*/
void voicetest::epoch_to_date_time(uint32_t epoch)
{
 
    elapsed_second_s = epoch%60;    epoch /= 60;

    elapsed_minute_s = epoch%60;     epoch /= 60;

    elapsed_hour_s   = epoch%24;     epoch /= 24;

    float years = epoch/(365*4+1)*4;
    epoch %= 365*4+1;
   
   
    unsigned int year;
    
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    unsigned int month;
    
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }
   
    elapsed_year_s  = years+year;
    elapsed_month_s = month;
    elapsed_day_s   = (epoch-days[year][month]-1); //original code had no +1 
}
/*-----------------------------------------------------------------------------*
*  NAME
*      get_date_time
*
*  DESCRIPTION
*      Innput the countdown from the gloabal variables and output the current
*      date.
*  RETURNS
*      
*----------------------------------------------------------------------------*/
void voicetest::get_date_time(uint8_t yyy, uint8_t mmm_o, uint8_t ddd, uint8_t hhh, uint8_t mmm, uint8_t sss)
{
   /*Function Variables Declaration*/ 
uint8_t total_s = 0,
        total_m = 0, additional_m = 0,
        total_h = 0, additional_h = 0,
        total_d = 0, additional_d = 0,
        total_mo = 0, additional_mo = 0,
        additional_y = 0;
uint16_t total_y = 0;        
  
/*CALCULATE SECONDS*/
    total_s = 55 + sss;
   //Checks if the addition >= 60s
   //If so a minute is carried and added.
  if(total_s > 59)
    {
     additional_m = 1;
     s = total_s - 60;
    }
    else 
    {
     additional_m = 0;
     s = total_s;
    }
 /*END CALCULATE SECONDS*/
 /*CALCULATE MINUTES*/  
   //This condition will never be meet because minute_s is always <= 36 m
  // Just in case the 23 is ever increased this condition will be meet.
    if(additional_m == 1)
    {
        total_m = mmm + 23 + 1;
       
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          m = total_m;
          additional_h = 0;
        }
             
    }
    if(additional_m == 0)
    {

      total_m = mmm + 23;
      
        if(total_m > 59) //In this case total_m will be 60
        {
           m = total_m - 60; // mm will equal 0
           additional_h = 1; // an hour is added
        }
        else
        {
          additional_h = 0;
          m = total_m;
        }
    }    
 /*END CALCULATE MINUTES*/
 /*CALCULATE HOURS*/
    if(additional_h == 1)
    {
    
      total_h = hhh + 14 + 1;
      
      if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 1; // an day is added
        }
        else
        {
          h = total_h;
          additional_d = 0;
        }
             
    }
    if(additional_h == 0)
    {
      total_h = hhh + 14;
        if(total_h >= 24) //In this case total_m will be 60
        {
           h = total_h - 24; // mm will equal 0
           additional_d = 0; // an hour is added

        }
        else
        {
          additional_d = 0;
          h = total_h;
        }
    } 
    /*END CALCULATE HOURS*/
    /*CALCULATE DAY & MONTH*/
    if(additional_d == 1)
    {
      total_d = ddd + 11 + 1;

      if(total_d > 31)
      {
        d = total_d - 31;
        additional_mo = 1;
        
       }
          else
            {
              d = total_d;
              additional_mo = 0;
            
            }
    }
    if(additional_d == 0)
    {
      total_d = ddd + 11;
   
      if(total_d > 31)
      {
             
        d = total_d - 31;
        additional_mo = 1;
      
       }
          else
            {
              d = total_d;
              additional_mo = 0;
              
            }

    }    




    

    if(additional_mo == 1)
    {
      total_mo = mmm_o + 5 +1;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }                
       }
            else
            {
               mo = total_mo;
               additional_y = 0;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
            }

      
    }
    
    if(additional_mo == 0)
    {
      
    total_mo = mmm_o + 5;

      if(total_mo > 12)
      {
        mo = total_mo - 12;
        additional_y = 1;

             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }        

      }
            else
            {
               mo = total_mo;
               additional_y = 0;


             if(mo == 4 || mo == 6 || mo == 9 || mo == 11) // Months that have 30 days
               {
                 if (d > 30)
                 {
                   d = 30;
                 }
               }
  
            }
      
    }

    /*END CALCULATE DAY & MONTH/
    /*CALCULATE YEAR*/
     if(additional_y == 1)
     {
      
      y = 14 + 1 + yyy;
      total_y = 2000 + y;
 
        //Check for leap year and change date only for Feb

             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }


        


        
     }
     if(additional_y == 0)
     {
        
        y = 14 + yyy;
        total_y = 2000 + y; 

        //Check for leap year and change date only for Feb
             if(total_y%400 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               }  
             }
             else if(total_y%100 == 0) //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }
             else if(total_y%4 == 0) //Is a leap year 
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 29;
                 }
               } 
             }
             else //Not a leap year
             {
               if(mo == 2) 
               {
                 if (d > 28)
                 {
                   d = 28;
                 }
               } 
             }

       
     }

    
    /*END CALCULATE YEAR*/


   

    
    /*END CALCULATE DATE&TIME*/
  UTC_TIME.concat("20");
  UTC_TIME.concat(y);
  UTC_TIME.concat("/");
  UTC_TIME.concat(mo);
  UTC_TIME.concat("/");
  UTC_TIME.concat(d);
  UTC_TIME.concat(" ");
  UTC_TIME.concat(h);
  UTC_TIME.concat(":");
  UTC_TIME.concat(m);
  UTC_TIME.concat(":");
  UTC_TIME.concat(s);
    
    
}
/*-----------------------------------------------------------------------------*
*  NAME
*      set_UTC
*
*  DESCRIPTION
*       Input the returend value from the -MSSTM command and the final output will 
*       be the UTC_TIME string equal to the current time.
*  RETURNS
*
*----------------------------------------------------------------------------*/  
  void voicetest::set_UTC(double epoch_val)
  {
     uint32_t epoch = epoch_val * 0.09;
     
     LFRAME.remove(0);
     LFRAME.concat(String(epoch_val,0));
     
     epoch_to_date_time(epoch);
     
     get_date_time(elapsed_year_s,elapsed_month_s,elapsed_day_s,elapsed_hour_s,elapsed_minute_s,elapsed_second_s);
    
     
  }
/*-----------------------------------------------------------------------------*
*  NAME
*      voice_call_dial
*
*  DESCRIPTION
*       Sets the global variable dial_at with the at command to dial a number
*   Ex: ATD1234567890
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::voice_call_dial(void)
{

  DIAL.concat("ATD");
  DIAL.concat(PHONE_NUMBER);
  DIAL.concat(";\r");

}

/*-----------------------------------------------------------------------------*
*  NAME
*      clear_data_array
*
*  DESCRIPTION
*        Sets all the values in DATA_ARRAY[] to NULL.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::clear_data_array(void)
{
  DATA_ARRAY.remove(0);
  DATA_ARRAY_TWO.remove(0);
}
/*-----------------------------------------------------------------------------*
*  NAME
*      init_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the header content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::init_logfile_string(void)
{
   clear_data_array();

   DATA_ARRAY.concat("DPL Statistics\nTest Start Time (UTC):  ");
   DATA_ARRAY.concat(UTC_TIME);
   DATA_ARRAY.concat("\n-------------------------------------------------------------------------");
   DATA_ARRAY.concat("UTC Time              LFrame      Phone Number      Att#  SVID  BeamID  ChanAsg"); 
   DATA_ARRAY_TWO.concat("    Access  Access     Setup  Setup      Comp  Duration  AbTerm  LGCX   LGCY   LGCZ   Audilog\n");
   DATA_ARRAY_TWO.concat("                                                          Init  Init    Time(sec)          "); 
   DATA_ARRAY_TWO.concat("Time(sec)         Time(sec)        (sec)\n");
      
}
/*-----------------------------------------------------------------------------*
*  NAME
*      logfile_upload_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the log content of the log
*    file. The input att represents the current att number. 
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::logfile_upload_string(int att)
{
  
  clear_data_array();
  
  DATA_ARRAY.concat(UTC_TIME);
  for(int i=0;i<(21-(UTC_TIME.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(LFRAME);
  for(int i=0;i<(11-(LFRAME.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(PHONE_NUMBER);
  for(int i=0;i<(18-(PHONE_NUMBER.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(String(att));
  for(int i=0;i<(6-((String(att)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(SVID);
  for(int i=0;i<(6-(SVID.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(BeamID);
  for(int i=0;i<(8-(BeamID.length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }
  
  DATA_ARRAY.concat(ACC);
  DATA_ARRAY.concat("       ");
  
  DATA_ARRAY.concat(String(ACCESS_TIME,2));
  for(int i=0;i<(11-((String(ACCESS_TIME,2)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY.concat(SET);
  DATA_ARRAY.concat("      ");
  
  DATA_ARRAY.concat(String(SETUP_TIME,2));
  for(int i=0;i<(11-((String(SETUP_TIME,2)).length()));i++)
  {
  DATA_ARRAY.concat(" ");
  }

  DATA_ARRAY_TWO.concat(COMP);
  DATA_ARRAY_TWO.concat("     ");
  DATA_ARRAY_TWO.concat(String(DURATION_TIME,2));
  for(int i=0;i<(9-((String(DURATION_TIME,2)).length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }


  DATA_ARRAY_TWO.concat(LGCX);
  for(int i=0;i<(7-(LGCX.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
  DATA_ARRAY_TWO.concat(LGCY);
  for(int i=0;i<(7-(LGCY.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
  DATA_ARRAY_TWO.concat(LGCZ);
  for(int i=0;i<(7-(LGCZ.length()));i++)
  {
  DATA_ARRAY_TWO.concat(" ");
  }
 DATA_ARRAY_TWO.concat("\n");
}
/*-----------------------------------------------------------------------------*
*  NAME
*      end_logfile_string
*
*  DESCRIPTION
*        Sets the gloabal variable DATA_ARRAY[] to the summary content of the log
*    file.
*  RETURNS
*
*----------------------------------------------------------------------------*/
void voicetest::end_logfile_string(void)
{
  
  

  clear_data_array();
  
  DATA_ARRAY.concat("\n\n\n\nTest Stop:    ");
  DATA_ARRAY.concat(UTC_TIME);
  DATA_ARRAY.concat("\n-----------------------\n");

  DATA_ARRAY.concat("Attempts      =    ");
  DATA_ARRAY.concat(String(ATTEMPTS));
  DATA_ARRAY.concat("\n");
  
  DATA_ARRAY.concat("Setups        =    ");
  DATA_ARRAY.concat(String(SETUPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Setup Pct     =  ");
  DATA_ARRAY.concat(String(SETUPS_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Connects      =    ");
  DATA_ARRAY.concat(String(CONNECTS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Connect Pct   =  ");
  DATA_ARRAY.concat(String(CONNECT_PCT,2));
  DATA_ARRAY.concat("%\n");

  DATA_ARRAY.concat("Drops         =    ");
  DATA_ARRAY.concat(String(DROPS));
  DATA_ARRAY.concat("\n");

  DATA_ARRAY.concat("Drop Pct      =  ");
  DATA_ARRAY.concat(String(DROPS_PCT,2));
  DATA_ARRAY.concat("%\n");
  DATA_ARRAY.concat("\n");
  
}
