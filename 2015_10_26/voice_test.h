/******************************************************************************
 *  (C) COPYRIGHT Letsky Innovations 2015
 *
 *    FILE
 *      $Workfile: voice_test.h$
 *      $Revision:$
 *      $Author: Reinerio Milanes$
 *
 *  DESCRIPTION
 *      Defines the variables used by the voice_test.c file
 *
 ******************************************************************************/
 
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H
/*============================================================================*
    Interface Header Files
 *============================================================================*/

#include "windows.h"
#include "winbase.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <cstringt.h>
/*============================================================================*
    Private Defines
 *============================================================================*/
#define TPI_PARAM_IDX (TPI_OPCODE_IDX+TPI_OPCODE_SIZE)
#define TPI_OPCODE_IDX 0
#define TPI_OPCODE_SIZE 1

/*============================================================================*
    Public Defines
 *============================================================================*/
 #define DEFAULT_CALL_TIME	50
 #define DEFAULT_ATTEMPTS   1000
 #define TPI_RRCS_SV_ID_BYTE (TPI_PARAM_IDX + 8)
 #define TPI_RRCS_BEAM_ID_BYTE (TPI_PARAM_IDX + 9)
 #define TPI_RRCS_X_COORD_WORD (TPI_PARAM_IDX + 10)
 #define TPI_RRCS_Y_COORD_WORD (TPI_PARAM_IDX + 12)
 #define TPI_RRCS_Z_COORD_WORD (TPI_PARAM_IDX + 14)
/*============================================================================*
    Public Data
 *============================================================================*/
 short CALL_TIME = 0, 
 	   ATTEMPTS = 0,
       SETUPS = 0,
       SETUPS_PCT = 0,
       CONNECTS = 0,
       CONNECT_PCT = 0,
       DROPS = 0,
       DROPS_PCT = 0,
       ACCESS_TIME = 0,
       DURATION_TIME = 0,
	   SETUP_TIME = 0;
       
	 
 bool COMPLETE = false, SETUP = false;

string UTC_TIME_DEFAULT = "2001/01/01  01:01:01",
       UTC_TIME = "", 
       PHONE_NUMBER = "";
const string DEFAULT_PHONE_NUMBER "55555";       
	   
char DATA_ARRAY[255];	

time_t start_t_access, end_t_access, 
       start_t_setup, end_t_setup,
       start_t_duration, end_t_duration;
       
short diff_t_access,
      diff_t_setup,
      diff_t_duration;    
 
 /*============================================================================*
    Public Functions
 *============================================================================*/
extern void input_phone_number(string phonenum);
extern void input_call_time(short calltime);
extern void input_utc_time(string utctime);
extern void input_attempts_number(short attemptsnum);
extern int calc_setup_pct(int ttlsetups, int ttlattemps);
extern int calc_connect_pct(int ttlconnect, int ttlattemps);
extern int calc_drop_pct(int ttldrop, int ttlconnect);
extern void access_watch_start(void);
extern int access_watch_stop(void);
extern void setup_watch_start(void);
extern int setup_watch_stop(void);
extern void duration_watch_start(void);
extern int duration_watch_stop(void);

#endif /* ndef _VOICE_TEST_H */
 
/*============================================================================*
    End Of File
 *============================================================================*/
