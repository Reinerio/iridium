//#include <Chrono.h>
#include <voice_test.h>
#include<CountUpDownTimer.h>

CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 

voicetest vt;
  int tries = 0;
  
String buffer_1 = "";
  
void setup() 
{
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB
  }
  t_a.StartTimer();
  t_s.StartTimer();
  t_d.StartTimer(); 

  do
  {
  
  if (Serial.available() > 0)
  {
    Serial.write("AT");
    buffer_1 = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(buffer_1);
  }
  
  }while(buffer_1 != "OK");

  vt.input_phone_number("12345678");
  vt.input_call_time(5);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(5);
  vt.BeamID = "12";
  vt.SVID = "09";
  vt.ACC = "Y";
  vt.SET = "Y";
  vt.COMP = "Y";
  vt.LGCX = "107.5";
  vt.LGCY = "98.3";
  vt.LGCZ = "104.3";

}

void loop()
{
  int attempt_number = 0;
  /*Send UTC_TIME string value to set ISU system time*/
  do
  {
  
  if (Serial.available() > 0)
  {
    //Serial.write(&vt.RTC);
    buffer_1 = Serial.readString();
    Serial.print("RTC setting sent, received: ");
    Serial.println(buffer_1);
  }
  
  }while(buffer_1 != "OK");

   if(buffer_1 == "OK")
     {
       vt.SET = "Y";
       vt.SETUPS++;
     }
      else
        vt.SET = "N";
        
      buffer_1.remove(0);
  
  /*Get UTC updated every time you log into file*/
  do
  {
  if (Serial.available() > 0)
  {
    Serial.write("AT+CCLK?");
    buffer_1 = Serial.readString();
    Serial.print("UPDATE UTC time sent, received: ");
    Serial.println(buffer_1);
  }
  }while(buffer_1 != "OK"); 
  
  vt.RTC.concat(buffer_1);
  buffer_1.remove(0);

  vt.init_logfile_string();
  Serial.print(vt.DATA_ARRAY);
  t_s.StopTimer(); 
  
  for(int i=0;i<vt.ATTEMPTS;i++)
  { 
    
  t_s.Timer();
  t_a.Timer();
  
  do
  {
  
  if (Serial.available() > 0)
  {
    Serial.write("AT");
    buffer_1 = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(buffer_1);
  }
  
  }while(buffer_1 != "OK");

  buffer_1.remove(0);
  
  vt.SETUP_TIME = t_s.ShowSeconds(); 
  t_s.StopTimer(); 

  t_a.Timer();
  
  do
  {
    
  if (Serial.available() > 0)
  {
    //Serial.write(vt.DIAL);
    buffer_1 = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(buffer_1);
  }
      if(buffer_1 == "OK")
      {
        vt.ACC = "Y";
        vt.CONNECTS++;
      }
      if(buffer_1 == "NO CARRIER")
      {
        vt.ACC = "N";
        vt.DROPS++;
      }

      
  } while(buffer_1 != "OK");

  buffer_1.remove(0);
  
  vt.ACCESS_TIME = t_a.ShowSeconds(); 
  t_a.StopTimer(); 
   
  
  t_d.Timer();
  if(vt.ACC == "Y")
  {
    
   while(vt.DURATION_TIME != vt.CALL_TIME)
    {
      if (t_d.TimeHasChanged())
      {
         vt.DURATION_TIME = t_d.ShowSeconds(); 
      }
    }

  if (Serial.available() > 0)
  {
    Serial.write("ATH");
    buffer_1 = Serial.readString();
    Serial.print("AT sent, received: ");
    Serial.println(buffer_1);
  }
  
  } while(buffer_1 != "OK");
  buffer_1.remove(0);
  
  vt.DURATION_TIME = t_d.ShowSeconds(); 
  t_d.StopTimer();
  
    vt.COMP = "Y";
  
  if(vt.ACC == "N")
    {
       vt.COMP = "N";      
    }
  
  /*
  * In this section of the code we will get the BEAMID SVID, X, Y, Z and frame 
  * values from the DPL PORT. For testing we will use the pre-defined values
  * set on the setup() function.
   */
   
  /*Get UTC updated every time you log into file*/
  do
  {
  if (Serial.available() > 0)
  {
    Serial.write("AT+CCLK?");
    buffer_1 = Serial.readString();
    Serial.print("UPDATE UTC time sent, received: ");
    Serial.println(buffer_1);
  }
  } while(buffer_1 != "OK"); 
  
  vt.RTC.concat(buffer_1);
  buffer_1.remove(0);

  
   vt.logfile_upload_string(attempt_number);
   Serial.println(vt.DATA_ARRAY);
 //  T.ResetTimer(); 
   vt.ACCESS_TIME = 0;
   vt.DURATION_TIME = 0;
   vt.SETUP_TIME = 0;

    attempt_number++;
  }
  
  /*Get UTC updated every time you log into file*/
  do
  {
  if (Serial.available() > 0)
  {
    Serial.write("AT+CCLK?");
    buffer_1 = Serial.readString();
    Serial.print("UPDATE UTC time sent, received: ");
    Serial.println(buffer_1);
  }
  }while(buffer_1 != "OK"); 
  
  vt.RTC.concat(buffer_1);
  buffer_1.remove(0);


  vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
  vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
  vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.CONNECTS);
  
  vt.end_logfile_string();
  Serial.print(vt.DATA_ARRAY);  













}




