#include <CountUpDownTimer.h>

#include <SoftwareSerial.h>

String DF_RX_buffer = "";
String dumDF_RX_buffer = "Hello World";
int set_up = 0, access = 0, duration = 0;
//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
int dial_fail_attempts = 5, time_attempt = 0;
uint32_t epoch;
SoftwareSerial DF_Port(10,6); //SoftwareSerial mySerial(RX, TX);
SoftwareSerial Dummy_Port(15,14);
void get_AT_Commands(void);
CountUpDownTimer t_a(UP),t_s(UP),t_d(UP); 

void setup() {
  
  // initialize serial:
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DF_Port.begin(57600);
  DF_Port.listen();
}

void loop() {

  Serial.println("AT init");
   do
   {
    Serial.println("ATD5555;\r");
   DF_Port.write("ATD5555;\r"); 
    //If > 0 means that there's data on the buffer
    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();//readStringUntil('-');
    }
       Serial.println("Buffer:");
       Serial.println(DF_RX_buffer);
         
    if(DF_RX_buffer.indexOf("OK") >= 0)
    {
        Serial.println("ATD5555");
        DF_RX_buffer.remove(0);
        DF_RX_buffer.concat("OK");
         
    }
    
     
    delay(2000);
   }while(DF_RX_buffer != "OK");

while(1)
{
   Serial.println("AT+clcc");
  DF_Port.write("AT+CLCC\r"); 
    //If > 0 means that there's data on the buffer
    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();//readStringUntil('-');
    }
     Serial.print("Buffer: ");
     Serial.print(DF_RX_buffer);
          
     DF_RX_buffer.remove(0);
     
    delay(2000);

    if(DF_RX_buffer.indexOf("000") >= 0)
    {
        Serial.println("ATH\r");
        DF_Port.write("ATH\r");
        while(1)
        {
           
        }
         
    }
  
    if(DF_RX_buffer.indexOf("001") >= 0)
    {
        Serial.println("Call held");
        DF_Port.write("ATH\r");
        while(1)
        {
           
        }
         
    }

    if(DF_RX_buffer.indexOf("002") >= 0)
    {
        Serial.println("Dialing");
        DF_Port.write("ATH\r");
        while(1)
        {
           
        }
         
    }

    if(DF_RX_buffer.indexOf("004") >= 0)
    {
        Serial.println("Incoming");
        DF_Port.write("ATH\r");
        while(1)
        {
           
        }
         
    }        
}
             
}

void get_AT_Commands(char timer)
{
   
   do
   {
   DF_Port.write("AT+CGMM\r"); 

     if(timer == 'S')
     {
      t_s.Timer();
     }
     if(timer == 'A')
     {
      t_a.Timer();
     }
     if(timer == 'D')
     {
      t_d.Timer();
     }


    //If > 0 means that there's data on the buffer
    while (DF_Port.available()>0)
    {
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
     
     DF_RX_buffer = DF_Port.readString();//readStringUntil('-');
    }
    Serial.print("Rx recived Data: ");  
    Serial.println(DF_RX_buffer);
    
    if(DF_RX_buffer.indexOf("9523") >= 0)
    {
       //Means that there's no network service
        Serial.println("Board 9523"); 
    }
    else
    {
      Serial.println("Enteer else"); 

    }

    DF_RX_buffer.remove(0);

    //wait 3 s and send data again 
    delay(2000);
    //delay(20);
   }while(DF_RX_buffer != "OK");
//       t_s.Timer();
//       t_a.Timer();
//       t_d.Timer();
   DF_RX_buffer.remove(0);
   
}

