#include <Chrono.h>
#include <voice_test.h>

voicetest vt;

void setup() 
{
  Serial.begin(9600);
  vt.SETUPS = 967;
  vt.CONNECTS = 800;
  vt.DROPS = 6;
  vt.access_watch_start();
  vt.setup_watch_start();
  vt.duration_watch_start();
  vt.input_phone_number("12345678");
  vt.input_call_time(0);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(0);
  vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
  vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
  vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.CONNECTS);
  delay(1111);
  vt.setup_watch_stop();
  Serial.println("Calculating data.........");
  delay(5000);
  vt.access_watch_stop();
  Serial.print("Phone #: ");
  Serial.println(vt.PHONE_NUMBER);
  Serial.print("UTC: ");
  Serial.println(vt.UTC_TIME);
  Serial.print("Call duration: ");
  Serial.println(vt.CALL_TIME);
  Serial.print("Att#: ");
  Serial.println(vt.ATTEMPTS);
    
  delay(3000);
  vt.duration_watch_stop();
  
  vt.init_logfile_string();
  Serial.print(vt.DATA_ARRAY);
  vt.logfile_upload_string();
  Serial.print(vt.DATA_ARRAY);
  vt.end_logfile_string();
  Serial.print(vt.DATA_ARRAY);  


}

void loop() {
  // put your main code here, to run repeatedly:

}
