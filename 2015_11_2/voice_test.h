/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: voice_test.c$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Defines the variables and functions used by the voice_test.c file
*
******************************************************************************/
#ifndef _VOICE_TEST_H
#define _VOICE_TEST_H

#include "windows.h"
#include "winbase.h"
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <cstring>
#include <stdlib.h>

/*============================================================================*
Private Defines
*============================================================================*/
#define TPI_PARAM_IDX (TPI_OPCODE_IDX+TPI_OPCODE_SIZE)
#define TPI_OPCODE_IDX 0
#define TPI_OPCODE_SIZE 1

/*============================================================================*
Public Defines
*============================================================================*/
#define DEFAULT_CALL_TIME	50
#define DEFAULT_ATTEMPTS   1000
#define MAX_CALL_TIME_SIZE 32767
#define TPI_RRCS_SV_ID_BYTE (TPI_PARAM_IDX + 8)
#define TPI_RRCS_BEAM_ID_BYTE (TPI_PARAM_IDX + 9)
#define TPI_RRCS_X_COORD_WORD (TPI_PARAM_IDX + 10)
#define TPI_RRCS_Y_COORD_WORD (TPI_PARAM_IDX + 12)
#define TPI_RRCS_Z_COORD_WORD (TPI_PARAM_IDX + 14) 

#ifdef __cplusplus 
extern "C" {
#endif 
	int ATTEMPTS = 0, SETUPS = 0, CONNECTS = 0, DROPS = 0;

	float
		CONNECT_PCT = 0,
		DROPS_PCT = 0,
		SETUPS_PCT = 0,
		ACCESS_TIME = 0,
		DURATION_TIME = 0,
		SETUP_TIME = 0,
		CALL_TIME = 0;

	bool COMPLETE = false, SETUP = false;

	char UTC_TIME_DEFAULT[] = "YY/MM/DD,HH:MM:SS",
		UTC_TIME[sizeof(UTC_TIME_DEFAULT)] = " ",
		MAX_PHONE_NUM_SIZE[] = "9999999999",
		PHONE_NUMBER[sizeof(MAX_PHONE_NUM_SIZE)] = "",
		attempp[5] = "",
		BeamID[8] = "",
		SVID[6] = "",
		ACC[6] = "",
		COMP[6] = "",
		SET[6] = "",
		LGCX[7] = "",
		LGCY[7] = "",
		LGCZ[7] = "",
		DIAL[sizeof(PHONE_NUMBER) + 4] = "",
		RTC[sizeof(UTC_TIME) + 10] = "";

	const char DEFAULT_PHONE_NUMBER[] = "5555";

	char DATA_ARRAY[255] = "";

	time_t start_t_access, end_t_access,
		start_t_setup, end_t_setup,
		start_t_duration, end_t_duration;

	short diff_t_access,
		diff_t_setup,
		diff_t_duration;

	/*============================================================================*
	Public Functions
	*============================================================================*/
	bool input_phone_number(char phonenum[]);
	bool input_call_time(float calltime);
	bool input_utc_time(char utctime[]);
	bool input_attempts_number(int attemptsnum);
	float calc_setup_pct(float ttlsetups, float ttlattemps);
	float calc_connect_pct(float ttlconnect, float ttlattemps);
	float calc_drop_pct(float ttldrop, float ttlconnect);

	extern void access_watch_start(void);
	extern void setup_watch_start(void);
	extern void duration_watch_start(void);
	extern int access_watch_stop(void);
	extern int setup_watch_stop(void);
	extern int duration_watch_stop(void);

	void voice_call_dial(void);
	void ISU_RTC_SET(void);

	void clear_data_array(void);
	void init_logfile_string(void);
	void end_logfile_string(void);
	void logfile_upload_string(void);


#ifdef __cplusplus 
}
#endif 

#endif /* _VOICE_TEST_H*/ 
/*============================================================================*
Example Code for usage of the main functions
*============================================================================*/
/*
	bool condition = false;
	int x;

	char str2[10] = "";
	char mystring[sizeof(MAX_PHONE_NUM_SIZE) + 3] = "";
	char mystring2[sizeof(UTC_TIME_DEFAULT) + 3] = "";
	cout << "Prog to test basic functions" << endl;
	cout << "Access, Setup, and Durations Timers started" << endl;

	access_watch_start();
	setup_watch_start();
	duration_watch_start();

	while (condition == false)
	{
		cout << "Format: 1234567890 " << endl;
		cout << "ENTER phone number : ";
		gets_s(mystring);
		condition = input_phone_number(mystring);
	}
	voice_call_dial();
	
	condition = false;

	while (condition == false)
	{
		cout << "Format: 00/01/01,24:01:00" << endl;
		cout << "Format: YY/MM/DD,HH:MM:SS" << endl;
		cout << "ENTER UTC Time: ";
		gets_s(mystring2);


		condition = input_utc_time(mystring2);
	}

	condition = false;

	while (condition == false)
	{
		float x;
		cout << "Max value 32767  0 = 50 default time" << endl;
		cout << "ENTER Call Duration: ";
		cin >> x;

		condition = input_call_time(x);
	}

	condition = false;

	while (condition == false)
	{
		float x;
		cout << "Max value 1000  0 = 1000 default attempts" << endl;
		cout << "ENTER # of Attempts: ";
		cin >> x;

		condition = input_attempts_number(x);
	}

	voice_call_dial();
	ISU_RTC_SET();

	SETUPS_PCT = calc_setup_pct(50, 0);
	CONNECT_PCT = calc_connect_pct(14, ATTEMPTS);
	DROPS_PCT = calc_drop_pct(0, 56);

	ACCESS_TIME = access_watch_stop();
	SETUP_TIME = setup_watch_stop();
	DURATION_TIME = duration_watch_stop();
	

	cout << "Resulst:  " << endl;
	cout << "Date: " << UTC_TIME << endl;
	cout << "Phone Number: " << PHONE_NUMBER << endl;
	cout << "Call Duration: " << CALL_TIME << endl;
	cout << "Number of Attempts: " << ATTEMPTS << endl;

	cout << "Setups: " << SETUPS_PCT << "%" << endl;
	cout << "Connects: " << CONNECT_PCT << "%" << endl;
	cout << "Drops: " << DROPS_PCT << "%" << endl;

	cout << "Access Timer: " << ACCESS_TIME << "s" << endl;
	cout << "Setup Timer: " << SETUP_TIME << "s" << endl;
	cout << "Duration Timer: " << DURATION_TIME << "s" << endl;

	init_logfile_string();
	end_logfile_string();
	logfile_upload_string();

	while (true)
	{

	}

	return 0;
*/


