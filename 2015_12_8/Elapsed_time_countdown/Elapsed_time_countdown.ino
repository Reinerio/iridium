
    unsigned char second_s; // 0-59
    unsigned char minute_s; // 0-59
    unsigned char hour_s;   // 0-23
    unsigned char day_s;    // 1-31
    unsigned char month_s;  // 1-12
    unsigned char year_s;   // 0-99 (representing 2000-2099)


static unsigned short days[4][12] =
{
    {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},
    { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},
    { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},
    {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},
};

uint32_t date_time_to_epoch(void);
void epoch_to_date_time(uint32_t epoch);

void setup() 
{



  Serial.begin(115200);

   second_s = 55;
   minute_s = 23;
   hour_s = 14;
   day_s = 11;
   month_s = 5;
   year_s = 2014;

   uint32_t epoch = 125976225;
    Serial.print("epoch: ");
  Serial.println(epoch);
  epoch_to_date_time(epoch);  
  
  Serial.print("Year: ");
  Serial.println(year_s);
  Serial.print("month: ");
  Serial.println(month_s);
  Serial.print("day: ");
  Serial.println(day_s);
  Serial.print("hour: ");
  Serial.println(hour_s);
  Serial.print("minute: ");
  Serial.println(minute_s);
  Serial.print("second: ");
  Serial.println(second_s);
    


  




    
}

void loop() 
{


     
  delay(2000);
}


uint32_t date_time_to_epoch()
{
    unsigned int second = second_s;  // 0-59
    unsigned int minute = minute_s;  // 0-59
    unsigned int hour   = hour_s;    // 0-23
    unsigned int day    = day_s-1;   // 0-30
    unsigned int month  = month_s-1; // 0-11
    unsigned int year   = year_s;    // 0-99
    return (((year/4*(365*4+1)+days[year%4][month]+day)*24+hour)*60+minute)*60+second;
}


void epoch_to_date_time(uint32_t epoch)
{
    second_s = epoch%60; epoch /= 60;
    minute_s = epoch%60; epoch /= 60;
    hour_s   = epoch%24; epoch /= 24;

    unsigned int years = epoch/(365*4+1)*4; epoch %= 365*4+1;

    unsigned int year;
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    unsigned int month;
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }

    year_s  = years+year;
    month_s = month;
    day_s   = epoch-days[year][month]-1;
}



