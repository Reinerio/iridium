#include <Chrono.h>
#include <voice_test.h>
#include<CountUpDownTimer.h>

CountUpDownTimer T(UP);

voicetest vt;
  int tries = 0;
  
void setup() 
{
  Serial.begin(115200);

  vt.setup_watch_start();
  vt.input_phone_number("12345678");
  vt.input_call_time(5);
  vt.input_utc_time("15/11/3,05:14:00");
  vt.input_attempts_number(5);
  vt.BeamID = "12";
  vt.SVID = "09";
  vt.ACC = "Y";
  vt.SET = "Y";
  vt.COMP = "Y";
  vt.LGCX = "107.5";
  vt.LGCY = "98.3";
  vt.LGCZ = "104.3";
  /*Send UTC_TIME string value to set ISU system time*/

//  delay(1111);

  Serial.println("Calculating data.........");

  Serial.print("Phone #: ");
  Serial.println(vt.PHONE_NUMBER);
  Serial.print("UTC: ");
  Serial.println(vt.UTC_TIME);
  Serial.print("Call duration: ");
  Serial.println(vt.CALL_TIME);
  Serial.print("Att#: ");
  Serial.println(vt.ATTEMPTS);
    
//  delay(3000);
//  
  

//  vt.logfile_upload_string();
//  Serial.print(vt.DATA_ARRAY);
//  vt.end_logfile_string();
//  Serial.print(vt.DATA_ARRAY);  
   vt.setup_watch_stop();
   T.StartTimer();
}

void loop()
{
   vt.access_watch_start();
  /*
  ADD setup section in here for Future User input
  */

  //after user all setting have been set
  

  tries = 0;
  
  /*After USART communication has been established and an 'OK' has been resibed from the ISU*/
  vt.access_watch_stop();
  
   vt.init_logfile_string();
  Serial.print(vt.DATA_ARRAY);
do
{
 vt.duration_watch_start();
  do
  {  T.Timer();
      if (T.TimeHasChanged() )
      {

      
      
      
      
      }
      
  }while(T.ShowSeconds()!=vt.CALL_TIME);

tries++;  

   vt.duration_watch_stop();
   vt.logfile_upload_string(tries);
   Serial.println(vt.DATA_ARRAY);
   T.ResetTimer(); 
   vt.ACCESS_TIME = 0;
   vt.DURATION_TIME = 0;
   vt.SETUP_TIME = 0;

}while(tries!=(vt.ATTEMPTS+1));

  vt.SETUPS_PCT = vt.calc_setup_pct(vt.SETUPS,vt.ATTEMPTS);
  vt.CONNECT_PCT = vt.calc_connect_pct(vt.CONNECTS,vt.ATTEMPTS);
  vt.DROPS_PCT = vt.calc_drop_pct(vt.DROPS,vt.CONNECTS);
  
  vt.end_logfile_string();
  Serial.print(vt.DATA_ARRAY);  





}

