#include <CountUpDownTimer.h>
#include <SoftwareSerial.h>

String DF_RX_buffer = "";

int set_up = 0, access = 0, duration = 0, attempts = 5, countdown = 0;
//PIN 7 set to RX << DF_RX
//PIN 6 set to TX >> DF_TX
SoftwareSerial DF_Port(10,6); //SoftwareSerial mySerial(RX, TX);

void set_add_type(void);
void set_msg_format(void);
void set_SMSC_add(void);

void setup() {
  // initialize serial:
  Serial.begin(57600);
  while (!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  // set the data rate for the SoftwareSerial port
  DF_Port.begin(57600);
  DF_Port.listen();
}

void loop()
{

  /***SETUP***/
     set_add_type();
     delay(500);
     set_msg_format();
     delay(500);
     set_SMSC_add();
     delay(500);
  /***END***SETUP***/
  do{
   DF_Port.write("AT-MSSTM\r"); 

    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }

    Serial.print("Buffer: ");
    Serial.println(DF_RX_buffer); 
    
    if(DF_RX_buffer.indexOf("OK") >= 0)
    {
      DF_RX_buffer.remove(0);
      DF_RX_buffer.concat("OK");
    }
    
   }while(DF_RX_buffer != "OK"); 
   DF_Port.write("AT+CIER=1,0,1\r"); 
   
  while(1)
  {
    
    
    
    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }

if(DF_RX_buffer.indexOf("+CIEV:1,1") >= 0)
  {


    
    DF_Port.write("AT+CMGS=23\r");
    countdown = 0;
    DF_RX_buffer.remove(0);
   do
   {
    
    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }
    
    if(DF_RX_buffer.indexOf(">") >= 0)
       {
        DF_RX_buffer.remove(0);
        // SMSC = 5555
        // Receiver = 1234567
        // Alphabet Size = 7
        // MSG = Hello World!
        DF_Port.write("0381555511000781214365F70000AA0CC8329BFD065DDF72363904");
        //DF_Port.write("0011000B816407281553F80000AA01E8329BFD4697D9EC37"); 
        DF_Port.write(0x1A); //26 = ctrl+z
       }
       DF_RX_buffer.remove(0);
       do{
            while (DF_Port.available()>0)
                  {
                   DF_RX_buffer = DF_Port.readString();
                  }
                 if(DF_RX_buffer.indexOf("+CMS ERROR") >= 0)
                   {
                       Serial.println("+CMS ERROR"); 
                       DF_RX_buffer.remove(0);
                       DF_RX_buffer.concat("OK");
                    }
       }while(DF_RX_buffer != "OK");  
    delay(1000);

   }while(DF_RX_buffer != "OK");

    countdown = 0;
    DF_RX_buffer.remove(0);
  }

  if(DF_RX_buffer.indexOf("+CIEV:1,0") >= 0)
  {
    Serial.println("FAIL NO NETWORK CONNECTION "); 
    DF_Port.write("AT+CIER=1\r"); 
  }
   else
  {
    
    DF_Port.write("AT+CIER=1\r"); 
  }

}

}

void set_add_type(void)
{
   do
   {
    
    DF_Port.write("AT+CSTA=129\r"); 

    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }
    Serial.print("AT+CSTA=129 Buffer: ");
    Serial.println(DF_RX_buffer);
    if(DF_RX_buffer.indexOf("OK") >= 0)
       {
       //Means that there's no network service
        Serial.println("AT+CSTA=129"); 
        DF_RX_buffer.remove(0);
        DF_RX_buffer.concat("OK");
        }
    else
        {
                  //"Recived NO CARRIER"
                  DF_RX_buffer.remove(0);
                  delay(1000);
                  countdown++;
                        if(countdown >= attempts) //try 5 times equals 5 seconds
                          {
                             Serial.println("AT+CSTA=129   FAIL");               
                             DF_RX_buffer.remove(0);
                             DF_RX_buffer.concat("OK");
                           }

            }


    delay(1000);
    
   }while(DF_RX_buffer != "OK");

    countdown = 0;
    DF_RX_buffer.remove(0);  
}
void set_msg_format(void)
{
   do
   {
    
    DF_Port.write("AT+CMGF=0\r"); 

    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }
    Serial.print("AT+CMGF=0 Buffer: ");
    Serial.println(DF_RX_buffer);
        
    if(DF_RX_buffer.indexOf("OK") >= 0)
       {
       //Means that there's no network service
        Serial.println("AT+CMGF=0"); 
        DF_RX_buffer.remove(0);
        DF_RX_buffer.concat("OK");
        }
    else
        {
                  //"Recived NO CARRIER"
                  DF_RX_buffer.remove(0);
                  delay(1000);
                  countdown++;
                        if(countdown >= attempts) //try 5 times equals 5 seconds
                          {
                             Serial.println("AT+CMGF=0   FAIL"); 
                             DF_RX_buffer.remove(0);
                             DF_RX_buffer.concat("OK");
                           }

            }


    delay(1000);
    
   }while(DF_RX_buffer != "OK");

    countdown = 0;
    DF_RX_buffer.remove(0);    
}

void set_SMSC_add(void)
{
   do
   {
    
    DF_Port.write("AT+CSCA=\"5555\"\r"); 

    while (DF_Port.available()>0)
    {
     DF_RX_buffer = DF_Port.readString();
    }
    Serial.print("AT+CSCA=\"5555\" Buffer: ");
    Serial.println(DF_RX_buffer);    
    if(DF_RX_buffer.indexOf("OK") >= 0)
       {
       //Means that there's no network service
        Serial.println("AT+CSCA=\"5555\""); 
        DF_RX_buffer.remove(0);
        DF_RX_buffer.concat("OK");
        }
    else
        {
                 DF_RX_buffer.remove(0);
                  delay(1000);
                  countdown++;
                        if(countdown >= attempts) //try 5 times equals 5 seconds
                          {
                             Serial.println("AT+CSCA=\"5555\"   FAIL");                             
                             DF_RX_buffer.remove(0);
                             DF_RX_buffer.concat("OK");
                           }

            }


    delay(1000);
    
   }while(DF_RX_buffer != "OK");

    countdown = 0;
    DF_RX_buffer.remove(0);   
}


