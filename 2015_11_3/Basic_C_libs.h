/******************************************************************************
*  (C) COPYRIGHT Letsky Innovations 2015
*
*    FILE
*      $Workfile: Basic_C_libs.h$
*      $Revision: 1.1$
*      $Author: Reinerio Milanes$
*
*  DESCRIPTION
*      Calls all the libs needed
*
******************************************************************************/
#ifndef _BASIC_C_LIBS_H
#define _BASIC_C_LIBS_H


/*============================================================================*
Private Defines
*============================================================================*/

/*============================================================================*
Public Defines
*============================================================================*/
//#include "utility/windows.h"
//#include "utility/winbase.h"
#include "string.h"
#include "stdio.h"
#include "time.h"
#include "iostream"
#include "cstring"
#include "stdlib.h"

#ifdef __cplusplus 
extern "C" {
#endif 

/*============================================================================*
	Public Functions
*============================================================================*/

#ifdef __cplusplus 
}
#endif 

#endif /* _BASIC_C_LIBS_H*/ 



